-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2017 at 04:53 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `virtual_class`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(20) NOT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email_verified` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `email`, `password`, `email_verified`) VALUES
(12, 'Admin', 'aziztareq295@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'c3f805fd0df65145b84b519d12a51c93'),
(13, 'Admin', 'virtualclass411@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Yes'),
(14, 'shantho', 'sirajul173013@gmail.com', '123456', '7cbf4c642570ded58207d22695828b11');

-- --------------------------------------------------------

--
-- Table structure for table `c_programming_class`
--

CREATE TABLE `c_programming_class` (
  `id` int(20) NOT NULL,
  `class_no` varchar(200) NOT NULL,
  `topic` varchar(200) NOT NULL,
  `introduction` varchar(20000) NOT NULL,
  `video` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `javascript_class`
--

CREATE TABLE `javascript_class` (
  `id` int(20) NOT NULL,
  `class_no` varchar(200) NOT NULL,
  `topic` varchar(200) NOT NULL,
  `introduction` varchar(20000) NOT NULL,
  `video` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `my_course`
--

CREATE TABLE `my_course` (
  `id` int(20) NOT NULL,
  `course_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(20) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `phone_no` varchar(200) NOT NULL,
  `present_add` text NOT NULL,
  `permanent_add` text NOT NULL,
  `profession` varchar(200) NOT NULL,
  `gender` varchar(200) NOT NULL,
  `nationality` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `email_verified` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `first_name`, `last_name`, `username`, `email`, `password`, `phone_no`, `present_add`, `permanent_add`, `profession`, `gender`, `nationality`, `image`, `email_verified`) VALUES
(1, 'Tareq', 'Aziz', 'tareqaziz', 'aziztareq295@gmail.com', '1b7a0b65f391bd5523e5d2c228a5a821', '01817241063', 'Chittagong', 'Chittagong ', 'Student', 'Male', 'Bangladeshi', '150972615920160413_183003.jpg', '26e0c0526f5538a5fc369b0672073960'),
(2, 'Fardin', 'Khan', 'fardinahsan', 'fs.shafi253@gmail.com', '8715a011adc72b7a7a8c2730536f99c2', '01817241063', 'Chittagong', 'Chittagong', 'Student', 'Male', 'Bangladeshi', '150972699620160412_110246.jpg', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `student_course`
--

CREATE TABLE `student_course` (
  `id` int(20) NOT NULL,
  `student_id` int(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `course_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_course`
--

INSERT INTO `student_course` (`id`, `student_id`, `email`, `course_name`) VALUES
(1, 2, 'fs.shafi253@gmail.com', 'Web Development');

-- --------------------------------------------------------

--
-- Table structure for table `trainerpanel`
--

CREATE TABLE `trainerpanel` (
  `id` int(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `developer` varchar(200) NOT NULL,
  `profile_picture` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email_verified` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainerpanel`
--

INSERT INTO `trainerpanel` (`id`, `name`, `email`, `developer`, `profile_picture`, `password`, `email_verified`) VALUES
(1, 'Fardin Ahsan', 'fs.shafi253@gmail.com', 'C Programmer', '150887157220160407_170536.jpg', '9ac1382fd8fc4b631594aa135d16ad75', 'Yes'),
(2, 'Tareq Aziz', 'aziztareq295@gmail.com', 'Web Developer', '1510949620DSC_0078.JPG', '1b7a0b65f391bd5523e5d2c228a5a821', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `web_design_class`
--

CREATE TABLE `web_design_class` (
  `id` int(20) NOT NULL,
  `class_no` varchar(200) NOT NULL,
  `topic` varchar(200) NOT NULL,
  `introduction` varchar(20000) NOT NULL,
  `video` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `web_development_class`
--

CREATE TABLE `web_development_class` (
  `id` int(20) NOT NULL,
  `class_no` varchar(200) NOT NULL,
  `topic` varchar(200) NOT NULL,
  `introduction` varchar(20000) NOT NULL,
  `video` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_development_class`
--

INSERT INTO `web_development_class` (`id`, `class_no`, `topic`, `introduction`, `video`) VALUES
(6, '1', 'Web Development', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi sapien, molestie vel eros id, blandit pretium sapien. Nunc at augue malesuada purus varius vehicula. Morbi turpis magna, eleifend sit amet facilisis ac, congue sed orci. Morbi in nibh sollicitudin, interdum dolor eget, facilisis felis. Etiam mauris leo, rhoncus et urna in, pharetra fringilla neque. Sed eget elit lorem. Nunc maximus blandit lectus quis auctor. Aliquam rhoncus sagittis sem, non porttitor ante varius id. Phasellus fermentum vehicula leo sed porta. Aliquam eleifend erat eu justo maximus porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi sapien, molestie vel eros id, blandit pretium sapien. Nunc at augue malesuada purus varius vehicula. Morbi turpis magna, eleifend sit amet facilisis ac, congue sed orci. Morbi in nibh sollicitudin, interdum dolor eget, facilisis felis. Etiam mauris leo, rhoncus et urna in, pharetra fringilla neque. Sed eget elit lorem. Nunc maximus blandit lectus quis auctor. Aliquam rhoncus sagittis sem, non porttitor ante varius id. Phasellus fermentum vehicula leo sed porta. Aliquam eleifend erat eu justo maximus porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi sapien, molestie vel eros id, blandit pretium sapien. Nunc at augue malesuada purus varius vehicula. Morbi turpis magna, eleifend sit amet facilisis ac, congue sed orci. Morbi in nibh sollicitudin, interdum dolor eget, facilisis felis. Etiam mauris leo, rhoncus et urna in, pharetra fringilla neque. Sed eget elit lorem. Nunc maximus blandit lectus quis auctor. Aliquam rhoncus sagittis sem, non porttitor ante varius id. Phasellus fermentum vehicula leo sed porta. Aliquam eleifend erat eu justo maximus porta.', '1514058112SHUNNO - Jhoriye Dao ft Fuad Al Muqtadir [Official Music Video] - YouTube.MKV'),
(7, '2', 'Basic Syntex of PHP', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi sapien, molestie vel eros id, blandit pretium sapien. Nunc at augue malesuada purus varius vehicula. Morbi turpis magna, eleifend sit amet facilisis ac, congue sed orci. Morbi in nibh sollicitudin, interdum dolor eget, facilisis felis. Etiam mauris leo, rhoncus et urna in, pharetra fringilla neque. Sed eget elit lorem. Nunc maximus blandit lectus quis auctor. Aliquam rhoncus sagittis sem, non porttitor ante varius id. Phasellus fermentum vehicula leo sed porta. Aliquam eleifend erat eu justo maximus porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi sapien, molestie vel eros id, blandit pretium sapien. Nunc at augue malesuada purus varius vehicula. Morbi turpis magna, eleifend sit amet facilisis ac, congue sed orci. Morbi in nibh sollicitudin, interdum dolor eget, facilisis felis. Etiam mauris leo, rhoncus et urna in, pharetra fringilla neque. Sed eget elit lorem. Nunc maximus blandit lectus quis auctor. Aliquam rhoncus sagittis sem, non porttitor ante varius id. Phasellus fermentum vehicula leo sed porta. Aliquam eleifend erat eu justo maximus porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mi sapien, molestie vel eros id, blandit pretium sapien. Nunc at augue malesuada purus varius vehicula. Morbi turpis magna, eleifend sit amet facilisis ac, congue sed orci. Morbi in nibh sollicitudin, interdum dolor eget, facilisis felis. Etiam mauris leo, rhoncus et urna in, pharetra fringilla neque. Sed eget elit lorem. Nunc maximus blandit lectus quis auctor. Aliquam rhoncus sagittis sem, non porttitor ante varius id. Phasellus fermentum vehicula leo sed porta. Aliquam eleifend erat eu justo maximus porta.', '1514060179SHUNNO - Shudhu Amar ft. Apeiruss [Official Music Video] - YouTube.MKV');

-- --------------------------------------------------------

--
-- Table structure for table `web_development_comment`
--

CREATE TABLE `web_development_comment` (
  `id` int(20) NOT NULL,
  `username` varchar(200) NOT NULL,
  `profile_pic` varchar(200) NOT NULL,
  `class_no` varchar(200) NOT NULL,
  `comment` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_programming_class`
--
ALTER TABLE `c_programming_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `javascript_class`
--
ALTER TABLE `javascript_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_course`
--
ALTER TABLE `my_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_course`
--
ALTER TABLE `student_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainerpanel`
--
ALTER TABLE `trainerpanel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_design_class`
--
ALTER TABLE `web_design_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_development_class`
--
ALTER TABLE `web_development_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_development_comment`
--
ALTER TABLE `web_development_comment`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `c_programming_class`
--
ALTER TABLE `c_programming_class`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `javascript_class`
--
ALTER TABLE `javascript_class`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `my_course`
--
ALTER TABLE `my_course`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `student_course`
--
ALTER TABLE `student_course`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `trainerpanel`
--
ALTER TABLE `trainerpanel`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `web_design_class`
--
ALTER TABLE `web_design_class`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `web_development_class`
--
ALTER TABLE `web_development_class`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `web_development_comment`
--
ALTER TABLE `web_development_comment`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
