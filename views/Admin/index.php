<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Admin Panel</title>
    <link href="../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="head">
            <h2>Virtual Class</h2>
            <span><a href=""> Log Out </a></span>
        </div>
        <div class="content">
            <div class="header">
                <h1>Admin Panel</h1>
                <p>Change What Do You Want</p>
            </div>
            <div class="Items">
                <nav>
                    <ul>
                        <li><a href="">Trainer List</a></li>
                        <li><a href="">Course Edit</a></li>
                        <li><a href="">Student List</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="footer"></div>
    </div>
</div>
</body>
</html>