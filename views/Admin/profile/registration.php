<?php
include_once ("../../../vendor/autoload.php");

use App\Admin\Auth;
use App\Admin\Admin;
use App\Message\Message;
use App\Utility\Utility;



$auth = new Auth();
$status= $auth->setData($_POST)->is_exist();

if ($status){
    Message::setMessage("<div class='alert alert-danger'>
    <strong>Taken!</strong> Email has already been taken. </div>");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}else{
    $_POST['email_token'] = md5(uniqid(rand()));
    $admin = new Admin();
    $admin->setData($_POST)->store();

    require "../../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php";

    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug  = 0;
    $mail->SMTPAuth   = true;
    $mail->SMTPSecure = "ssl";
    $mail->Host       = "smtp.gmail.com";
    $mail->Port       = 465;
    $mail->AddAddress($_POST['email']);
    $mail->Username=" ai3482507@gmail.com";
    $mail->Password="00112233";
    $mail->SetFrom('ai3482507@gmail.com','Virtual Class');
    $mail->AddReplyTo("ai3482507@gmail.com","Virtual Class");
    $mail->Subject  = "Your Account Activation Link";

    $message= "
        Please click this link to verify your account:
        http://localhost/Virtual_Class/views/Admin/profile/emailverification.php?email=".$_POST['email']."&email_token=".$_POST['email_token'];

    $mail->MsgHTML($message);
    $mail->Send();

}