<?php
if (!isset($_SESSION)) session_start();

include_once ("../../../vendor/autoload.php");

use App\Admin\Admin;
use App\Message\Message;
use App\Utility\Utility;

if (isset($_POST['email'])){
    $admin = new Admin();
    $admin->setData($_POST);

    $singleuser= $admin->view();

    $passwordresetlink= $singleuser->password;

    require "../../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php";

    $mail= new PHPMailer();
    $mail-> IsSMTP();
    $mail->SMTPDebug  = 0;
    $mail->SMTPAuth   = true;
    $mail->SMTPSecure = "ssl";
    $mail->Host       = "smtp.gmail.com";
    $mail->Port       = 465;
    $mail->AddAddress($_POST['email']);
    $mail->username="virtualclass411@gmail.com";
    $mail->password="Virtual411@";
    $mail->SetFrom('virtualclass411@gmail.com', 'Virtual Class');
    $mail->AddReplyTo('virtualclass411@gmail.com', 'Virtual Class');
    $mail->Subject="Your Account Password reset Link";

    $message =  "Please click this link to reset your password: 
       http://localhost/Virtual_Class/views/Admin/profile/resetpassword.php?email=".$_POST['email']."&code=".$singleuser->password;

    $mail->MsgHTML($message);
    if($mail->Send()){

        Message::message("
                <div class=\"alert alert-success\">
                            <strong>Email Sent!</strong> Please check your email for password reset link.
                </div>");
    }


}