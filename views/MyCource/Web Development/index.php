<?php
if(!isset($_SESSION)) session_start();

include_once ("../../../vendor/autoload.php");

use App\User\Auth;
use App\TrainerPanel\ClassUpload;
use App\Utility\Utility;

$auth  = new Auth();

$obj1 = new ClassUpload();

$allData= $obj1->webdevelopment_index();


######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemPerPage = $_SESSION['ItemsPerPage'];
else   $itemPerPage = 1;
$_SESSION['ItemsPerPage']= $itemPerPage;

$pages = ceil($recordCount/$itemPerPage);
$someData = $obj1->webdevelopment_pagination($page,$itemPerPage);

$serial = (($page-1) * $itemPerPage) +1;

####################### pagination code block#1 of 2 end #########################################





?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Virtual Class</title>
    <link href="../../../resource/css/style.css" rel="stylesheet">
    <link href="../../../resource/css/Html&css.css" rel="stylesheet">
    <link href="../../../resource/fonts.css" rel="stylesheet">
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../../../resource/bootstrap/js/jquery.min.js"></script>
    <script src="../../../resource/js/jquery.fitvids.js"></script>
    <script>
        $(document).ready(function(){
            // Target your .container, .wrapper, .post, etc.
            $(".video").fitVids();
        });
    </script>
</head>


<body>
<div class="contain">
    <div class="header">
        <a href="#" id="logo"></a>
        <nav>
            <label for="drop" class="toggle">Menu</label>
            <input type="checkbox" id="drop" />
            <ul class="menu">
                <li><a href="">Home</a></li>
                <li><a href="">Traineer Panel</a></li>
                <li><a href="">My Account</a></li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-1" class="toggle-1">Sign Up +</label>
                    <a href="#" class="drp">Sign Up</a>
                    <input type="checkbox" id="drop-1"/>
                    <ul>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>

                    </ul>

                </li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-2" class="toggle-2">Sign In +</label>
                    <a href="#">Sign In</a>
                    <input type="checkbox" id="drop-2"/>
                    <ul>
                        <li><a href="" target="_blank">Admin</a></li>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>
                    </ul>

                </li>
            </ul>
        </nav>
    </div>
    <div class="content">
        <div class="top-side">
            <div class="logo">
                <img src="../../../resource/image/logo/logoo.png">
            </div>
            <div class="category">
                <nav class="nav">
                    <ul class="nav__menu">
                        <li class="nav__menu-item"><a>Course Category</a>
                            <ul class="nav__submenu">
                                <li class="nav__submenu-item"> <a href="">Programming Language C</a></li>
                                <li class="nav__submenu-item"> <a href="">Web Design Html & Css</a></li>
                                <li class="nav__submenu-item"> <a href="">Web Development PHP</a></li>
                                <li class="nav__submenu-item"> <a href="">JavaScript</a></li>
                            </ul>
                        </li>


                    </ul>
                </nav>
            </div>
            <div class="search">
                <input type="search" placeholder="Enter Your Keyword" name="search-here" class="search-box">
                <button type="submit" name="submit" class="submit">Search</button>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="main col-lg-12">
            <div class="sidebar col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="list">
                    <h1>Class</h1>
                    <ul>
                        <?php
                            $pageMinusOne=$page-1;
                            $pagePlusOne=$page+1;

                            if($page>$pages) Utility::redirect("index.php?=$pages");
                            for($i=1;$i<=$pages;$i++)
                            {
                                if($i==$page) echo '<li class="active"><a href="?Page=$i">'.'Class No ' .$i . '</a></li>';
                                else  echo "<li><a href='?Page=$i'>".'Class No '. $i . '</a></li>';

                            }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="intro col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <?php
                    foreach ($someData as $oneData){
                 echo "<div class=".'class-name">
                   <h1 style="text-align: center">'.$oneData['topic'].'</h1>
                   
                </div>
                <div class="introduce">
                    <h1>Introduction</h1>
                    <p>
                    '.$oneData['introduction'].'
                    </p>
                </div>
                <div class="video-info">
                    <video class="video" width="720" controls>
                        <source src="../../../resource/class_video/webdevelopment/'.$oneData['video'].'" type="video/mp4">
                    </video>
                </div>
                '.$serial++;

                }
                ?>
                <div class="comment-sec">
                    <h4>Comment here:</h4>
                    <div class="comment-area">
                        <form action="" method="post">
                            <img src="../../../resource/Uploads/ " class="thumbnail img-responsive">
                            <h6></h6>
                            <textarea typeof="text" name="comment" class="comment-box"></textarea>
                            <button name="button" type="submit">Comment</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="part">
        <div class="part-1">
            <img src="../../../resource/image/logo/vc.png">
        </div>
        <div class="part-2">
            <div class="contact-us">
                <h1>About Us</h1>
                <div class="info">
                    <p>BSPI Computer Engineering Project.</p>
                    <p>7th Semister, 2nd Shift.</p>
                    <p>Computer Department,</p>
                    <p>Bangladesh Sweden Polytechnic Institute.</p>
                    <p>Tel : 01752-111111, 01631-222222</p>
                    <p><a href="">https://virtualclass-new.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="part-3">
        <div class="social">
            <ul>
                <li><a href=""><i class="icon icon-facebook"></i></a></li>
                <li><a href=""><i class="icon icon-twitter"></i></a></li>
                <li><a href=""><i class="icon icon-instagram"></i></a></li>
                <li><a href=""><i class="icon icon-youtube"></i></a></li>
                <li><a href=""><i class="icon icon-google-plus3"></i></a></li>
            </ul>
        </div>
    </div>
</div>


</body>
</html>