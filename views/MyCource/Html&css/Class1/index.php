<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Virtual Class</title>
    <link href="../../../../resource/css/style.css" rel="stylesheet">
    <link href="../../../../resource/css/Html&css.css" rel="stylesheet">
    <link href="../../../../resource/fonts.css" rel="stylesheet">
    <link href="../../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../../../../resource/bootstrap/js/jquery.min.js"></script>
    <script src="../../../../resource/js/jquery.fitvids.js"></script>
    <script>
        $(document).ready(function(){
            // Target your .container, .wrapper, .post, etc.
            $(".video").fitVids();
        });
    </script>
</head>
<body>
<div class="contain">
    <div class="header">
        <a href="#" id="logo"></a>
        <nav>
            <label for="drop" class="toggle">Menu</label>
            <input type="checkbox" id="drop" />
            <ul class="menu">
                <li><a href="">Home</a></li>
                <li><a href="">Traineer Panel</a></li>
                <li><a href="">My Account</a></li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-1" class="toggle-1">Sign Up +</label>
                    <a href="#" class="drp">Sign Up</a>
                    <input type="checkbox" id="drop-1"/>
                    <ul>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>

                    </ul>

                </li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-2" class="toggle-2">Sign In +</label>
                    <a href="#">Sign In</a>
                    <input type="checkbox" id="drop-2"/>
                    <ul>
                        <li><a href="" target="_blank">Admin</a></li>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>
                    </ul>

                </li>
            </ul>
        </nav>
    </div>
    <div class="content">
        <div class="top-side">
            <div class="logo">
                <img src="../../../../resource/image/logo/logoo.png">
            </div>
            <div class="category">
                <nav class="nav">
                    <ul class="nav__menu">
                        <li class="nav__menu-item"><a>Course Category</a>
                            <ul class="nav__submenu">
                                <li class="nav__submenu-item"> <a href="">Programming Language C</a></li>
                                <li class="nav__submenu-item"> <a href="">Web Design Html & Css</a></li>
                                <li class="nav__submenu-item"> <a href="">Web Development PHP</a></li>
                                <li class="nav__submenu-item"> <a href="">JavaScript</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="search">
                <input type="search" placeholder="Enter Your Keyword" name="search-here" class="search-box">
                <button type="submit" name="submit" class="submit">Search</button>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="main col-lg-12">
            <div class="sidebar col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="list">
                    <h1>Class</h1>
                    <ul>
                        <li><a href="">Class No 1</a></li>
                        <li><a href="">Class No 2</a></li>
                        <li><a href="">Class No 3</a></li>
                        <li><a href="">Class No 4</a></li>
                        <li><a href="">Class No 5</a></li>
                        <li><a href="">Class No 6</a></li>
                        <li><a href="">Class No 7</a></li>
                        <li><a href="">Class No 8</a></li>
                        <li><a href="">Class No 09</a></li>
                        <li><a href="">Class No 10</a></li>
                        <li><a href="">Class No 11</a></li>
                        <li><a href="">Class No 12</a></li>
                        <li><a href="">Class No 13</a></li>
                    </ul>
                </div>
            </div>
            <div class="intro col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="class-name">
                    <h1>Class No 1</h1>
                </div>
                <div class="introduce">
                    <h1>Introduction</h1>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Vivamus mi sapien, molestie vel eros id, blandit pretium sapien.
                        Nunc at augue malesuada purus varius vehicula. Morbi turpis magna,
                        eleifend sit amet facilisis ac, congue sed orci. Morbi in nibh sollicitudin,
                        interdum dolor eget, facilisis felis. Etiam mauris leo, rhoncus et urna in,
                        pharetra fringilla neque. Sed eget elit lorem. Nunc maximus blandit lectus quis auctor.
                        Aliquam rhoncus sagittis sem, non porttitor ante varius id. Phasellus fermentum vehicula leo sed porta.
                        Aliquam eleifend erat eu justo maximus porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Vivamus mi sapien, molestie vel eros id, blandit pretium sapien.
                        Nunc at augue malesuada purus varius vehicula. Morbi turpis magna,
                        eleifend sit amet facilisis ac, congue sed orci. Morbi in nibh sollicitudin,
                        interdum dolor eget, facilisis felis. Etiam mauris leo, rhoncus et urna in,
                        pharetra fringilla neque. Sed eget elit lorem. Nunc maximus blandit lectus quis auctor.
                        Aliquam rhoncus sagittis sem, non porttitor ante varius id. Phasellus fermentum vehicula leo sed porta.
                        Aliquam eleifend erat eu justo maximus porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Vivamus mi sapien, molestie vel eros id, blandit pretium sapien.
                        Nunc at augue malesuada purus varius vehicula. Morbi turpis magna,
                        eleifend sit amet facilisis ac, congue sed orci. Morbi in nibh sollicitudin,
                        interdum dolor eget, facilisis felis. Etiam mauris leo, rhoncus et urna in,
                        pharetra fringilla neque. Sed eget elit lorem. Nunc maximus blandit lectus quis auctor.
                        Aliquam rhoncus sagittis sem, non porttitor ante varius id. Phasellus fermentum vehicula leo sed porta.
                        Aliquam eleifend erat eu justo maximus porta.

                    </p>
                </div>
                <div class="video-info">
                    <video class="video" width="720" controls>
                        <source src="../../../../resource/class_video/html&css/class1/video/video.mp4" type="video/mp4">
                    </video>
                </div>
                <div class="comment col-xs-12">
                    <!-- Begin Comments JavaScript Code -->
                    <script type="text/javascript" async>
                        function ajaxpath_59d2aae3762d9(url)
                        {return window.location.href == '' ? url : url.replace('&s=','&s=' + escape(window.location.href));}
                        (function(){document.write('<div id="fcs_div_59d2aae3762d9">' +
                            '<a title="free comment script" href="http://www.freecommentscript.com">&nbsp;&nbsp;' +
                            '<b>Free HTML User Comments</b>...</a></div>');
                            fcs_59d2aae3762d9=document.createElement('script');
                            fcs_59d2aae3762d9.type="text/javascript";
                            fcs_59d2aae3762d9.src=ajaxpath_59d2aae3762d9((document.location.protocol=="https:"?"https:":"http:")
                                +"//www.freecommentscript.com/GetComments2.php?p=59d2aae3762d9&s=#!59d2aae3762d9");
                            setTimeout("document.getElementById('fcs_div_59d2aae3762d9').appendChild(fcs_59d2aae3762d9)",1);})();
                    </script>
                    <noscript>
                        <div>
                            <a href="http://www.freecommentscript.com" title="free html user comment box">Free Comment Script</a>
                        </div>
                    </noscript>
                    <!-- End Comments JavaScript Code -->
                </div>

            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="part">
        <div class="part-1">
            <img src="../../../../resource/image/logo/vc.png">
        </div>
        <div class="part-2">
            <div class="contact-us">
                <h1>. Us</h1>
                <div class="info">
                    <p>BSPI Computer Engineering Project.</p>
                    <p>7th Semister, 2nd Shift.</p>
                    <p>Computer Department,</p>
                    <p>Bangladesh Sweden Polytechnic Institute.</p>
                    <p>Tel : 01752-111111, 01631-222222</p>
                    <p><a href="">https://virtualclass-new.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="part-3">
        <div class="social">
            <ul>
                <li><a href=""><i class="icon icon-facebook"></i></a></li>
                <li><a href=""><i class="icon icon-twitter"></i></a></li>
                <li><a href=""><i class="icon icon-instagram"></i></a></li>
                <li><a href=""><i class="icon icon-youtube"></i></a></li>
                <li><a href=""><i class="icon icon-google-plus3"></i></a></li>
            </ul>
        </div>
    </div>
</div>


</body>
</html>