<?php

include_once ("../../../vendor/autoload.php");

if (!isset($_SESSION)) session_start();

use App\TrainerPanel\Auth;
use App\TrainerPanel\TrainerPanel;

$auth = new Auth();

$obj = new TrainerPanel();

$info = $obj->setData()->index();

?>
<!DOCTYPE html>
<html>
<head>
    <title>Trainer</title>
    <link href="../../../resource/css/style.css" rel="stylesheet">
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../resource/fonts.css" rel="stylesheet">
    <link href="../../../resource/css/trainerpro.css" rel="stylesheet">
</head>
<body>

<?php

$id = 0;
foreach ($info as $acc_item){
    $id++;
}

?>
<div class="contain">
    <div class="header">
        <a href="#" id="logo"></a>
        <nav>
            <label for="drop" class="toggle">Menu</label>
            <input type="checkbox" id="drop" />
            <ul class="menu">
                <li><a href="../../../index.php">Home</a></li>
                <li><a href="../../TrainerPanel/index.php">Traineer Panel</a></li>
                <li><a href="index.php">My Account</a></li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-1" class="toggle-1">Sign Up +</label>
                    <a href="#" class="drp">Sign Up</a>
                    <input type="checkbox" id="drop-1"/>
                    <ul>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>

                    </ul>

                </li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-2" class="toggle-2">Sign In +</label>
                    <a href="#">Sign In</a>
                    <input type="checkbox" id="drop-2"/>
                    <ul>
                        <li><a href="" target="_blank">Admin</a></li>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>
                    </ul>

                </li>
            </ul>
        </nav>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="main">
            <div class="view">
                <div class="heading">
                    <h2>My Account</h2>
                    <p>Account information. Upload your class. You can change your account information which you have to need</p>
                </div>
                <div class="account">
                    <div class="profile-head col-md-4 col-sm-4 col-xs-12">
                        <a href="" class="thumbnail">
                            <img src="<?php echo "../../../resource/image/Trainer/".$acc_item['profile_picture']?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="foot col-md-3 col-sm-6 col-xs-12">
                        <button class="acc-button" onclick="window.location='../Class_upload/Webdevelopment_Class_Upload.php'">Upload Your Class</button>
                        <button class="acc-button" onclick="window.location='Change_info.php'">Change Account Info</button>
                    </div>
                    <div class="profile-info">
                        <table class="col-md-10 col-sm-12 col-xs-12">
                            <tr class="">
                                <td class="">Name : </td>
                                <td><?php echo $acc_item['name']?></td>
                            </tr>
                            <tr>
                                <td>Email : </td>
                                <td><?php  echo $acc_item['email']?></td>
                            </tr>
                            <tr>
                                <td>Work as a : </td>
                                <td><?php echo $acc_item['developer']?></td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="part">
        <div class="part-1">
            <img src="../../../resource/image/logo/vc.png">
        </div>
        <div class="part-2">
            <div class="contact-us">
                <h1>Contact Us</h1>
                <div class="info">
                    <p>BSPI Computer Engineering Project.</p>
                    <p>7th Semister, 2nd Shift.</p>
                    <p>Computer Department,</p>
                    <p>Bangladesh Sweden Polytechnic Institute.</p>
                    <p>Tel : 01752-111111, 01631-222222</p>
                    <p><a href="">https://virtualclass-new.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="part-3">
        <div class="social">
            <ul>
                <li><a href=""><i class="icon icon-facebook"></i></a></li>
                <li><a href=""><i class="icon icon-twitter"></i></a></li>
                <li><a href=""><i class="icon icon-instagram"></i></a></li>
                <li><a href=""><i class="icon icon-youtube"></i></a></li>
                <li><a href=""><i class="icon icon-google-plus3"></i></a></li>
            </ul>
        </div>
    </div>
</div>
</body>
</html>