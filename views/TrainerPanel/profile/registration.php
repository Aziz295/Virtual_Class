<?php
include_once ("../../../vendor/autoload.php");

use App\TrainerPanel\Auth;
use App\TrainerPanel\TrainerPanel;
use App\Message\Message;
use App\Utility\Utility;


//for profile picture

if (isset($_FILES['profile_picture'])){
    $errors = array();
    $file_name= time().$_FILES['profile_picture']['name'];
    //$file_size= $_FILES['profile_picture']['size'];
    $file_tmp= $_FILES['profile_picture']['tmp_name'];
    $file_type = $_FILES['profile_picture']['type'];
    $file_ext = strtolower(end(explode('.',$_FILES['profile_picture']['name'])));

    $formats = array("jpeg","jpg","png");

    if (in_array($file_ext,$formats)===false){
        $errors[]="extension not allowed.. Please Choose a JPEG or PNG file";
    }
    /*if ($file_size>2019152){
        $errors[]= "File Must Be 2MB size";
    }*/
    if (empty($errors)==true){
        move_uploaded_file($file_tmp,"../../../resource/image/Trainer/".$file_name);
        $_POST['profile_picture']=$file_name;
    }else{
        print_r($errors);
    }
}


$auth = new Auth();
$status= $auth->setData($_POST)->is_exist();

if ($status){
    Message::setMessage("<div class='alert alert-danger'>
    <strong>Taken!</strong> Email has already been taken. </div>");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}else{
    $_POST['email_token'] = md5(uniqid(rand()));
    $trainer = new TrainerPanel();
    $trainer->setData($_POST)->store();

    require "../../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php";

    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug  = 0;
    $mail->SMTPAuth   = true;
    $mail->SMTPSecure = "ssl";
    $mail->Host       = "smtp.gmail.com";
    $mail->Port       = 465;
    $mail->AddAddress($_POST['email']);
    $mail->Username=" ai3482507@gmail.com";
    $mail->Password="00112233";
    $mail->SetFrom('ai3482507@gmail.com','Virtual Class');
    $mail->AddReplyTo("ai3482507@gmail.com","Virtual Class");
    $mail->Subject  = "Your Account Activation Link";

    $message= "
        Please click this link to verify your account:
        http://localhost/Virtual_Class/views/TrainerPanel/profile/emailverification.php?email=".$_POST['email']."&email_token=".$_POST['email_token'];

    $mail->MsgHTML($message);
    $mail->Send();

}



