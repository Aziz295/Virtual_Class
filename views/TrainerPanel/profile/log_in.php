<?php
include_once ("../../../vendor/autoload.php");

if (!isset($_SESSION)) session_start();

use App\Message\Message;

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Trainer Log</title>
    <link href="../../../resource/css/style.css" rel="stylesheet">
    <link href="../../../resource/css/adminlogin.css" rel="stylesheet">
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../resource/fonts.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="header">
    <a href="#" id="logo"></a>
    <nav>
        <label for="drop" class="toggle">Menu</label>
        <input type="checkbox" id="drop" />
        <ul class="menu">
            <li><a href="../../../index.php" >Home</a></li>
            <li><a href="../../../index.php">Traineer Panel</a></li>
            <li><a href="../../MyAccount/index.php">My Account</a></li>
            <li>
                <!-- First Tier Drop Down -->
                <label for="drop-1" class="toggle-1">Sign Up +</label>
                <a href="#" class="drp">Sign Up</a>
                <input type="checkbox" id="drop-1"/>
                <ul>
                    <li><a href="" target="_blank">Trainer</a></li>
                    <li><a href="" target="_blank">Student</a></li>

                </ul>

            </li>
            <li>
                <!-- First Tier Drop Down -->
                <label for="drop-2" class="toggle-2">Sign In +</label>
                <a href="#">Sign In</a>
                <input type="checkbox" id="drop-2"/>
                <ul>
                    <li><a href="" target="_blank">Admin</a></li>
                    <li><a href="" target="_blank">Trainer</a></li>
                    <li><a href="" target="_blank">Student</a></li>
                </ul>

            </li>
        </ul>
    </nav>
</div>
<div class="container">
    <div class="row">

        <table>
            <tr>
                <td width='230' >

                <td width='600' height="50" >


                    <?php  if(isset($_SESSION['message']) )if($_SESSION['message']!=""){ ?>

                        <div  id="message" class="form button"   style="font-size: smaller  " >
                            <center>
                                <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                                    echo "&nbsp;".Message::message();
                                }
                                Message::message(NULL);
                                ?></center>
                        </div>
                    <?php } ?>
                </td>
            </tr>
        </table>

        <div class="main col-md-4 col-sm-6 col-xs-12 col-sm-offset-4">
            <div class="head">
                <h1>Trainer Login</h1>
            </div>
            <form action="../Authentication/login.php" method="post">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" placeholder="Your Email" class="form-email form-control" id="form-email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" placeholder="Your password" class="form-password form-control" id="form-password">
                </div>
                <div class="option">
                    <a href="forgotten.php">Forget Password??</a>
                </div>
                <button type="submit" name="login" value="login" class="btn btn-primary">Login</button>

            </form>
        </div>
    </div>
</div>
<div class="footer">
    <div class="part">
        <div class="part-1">
            <img src="../../../resource/image/logo/vc.png">
        </div>
        <div class="part-2">
            <div class="contact-us">
                <h1>Contact Us</h1>
                <div class="info">
                    <p>BSPI Computer Engineering Project.</p>
                    <p>7th Semister, 2nd Shift.</p>
                    <p>Computer Department,</p>
                    <p>Bangladesh Sweden Polytechnic Institute.</p>
                    <p>Tel : 01752-111111, 01631-222222</p>
                    <p><a href="">https://virtualclass-new.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="part-3">
        <div class="social">
            <ul>
                <li><a href=""><i class="icon icon-facebook"></i></a></li>
                <li><a href=""><i class="icon icon-twitter"></i></a></li>
                <li><a href=""><i class="icon icon-instagram"></i></a></li>
                <li><a href=""><i class="icon icon-youtube"></i></a></li>
                <li><a href=""><i class="icon icon-google-plus3"></i></a></li>
            </ul>
        </div>
    </div>
</div>
</body>
</html>