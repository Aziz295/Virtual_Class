<?php

include_once ("../../../vendor/autoload.php");
if (!isset($_SESSION)) session_start();

use App\Message\Message;

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Trainer Log</title>
    <link href="../../../resource/css/style.css" rel="stylesheet">
    <link href="../../../resource/css/adminreg.css" rel="stylesheet">
    <link href="../../../resource/fonts.css" rel="stylesheet">
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="contain">
    <div class="header">
        <a href="#" id="logo"></a>
        <nav>
            <label for="drop" class="toggle">Menu</label>
            <input type="checkbox" id="drop" />
            <ul class="menu">
                <li><a href="../../../index.php">Home</a></li>
                <li><a href="../../../index.php">Traineer Panel</a></li>
                <li><a href="../../MyAccount/index.php">My Account</a></li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-1" class="toggle-1">Sign Up +</label>
                    <a href="" class="drp">Sign Up</a>
                    <input type="checkbox" id="drop-1"/>
                    <ul>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>

                    </ul>

                </li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-2" class="toggle-2">Sign In +</label>
                    <a href="">Sign In</a>
                    <input type="checkbox" id="drop-2"/>
                    <ul>
                        <li><a href="" target="_blank">Admin</a></li>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>
                    </ul>

                </li>
            </ul>
        </nav>
    </div>
</div>
<div class="container">

    <table>
        <tr>
            <td width='230' >

            <td width='600' height="50" >


                <?php  if(isset($_SESSION['message']) )if($_SESSION['message']!=""){ ?>

                    <div  id="message" class="form button"   style="font-size: smaller  " >
                        <center>
                            <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                                echo "&nbsp;".Message::message();
                            }
                            Message::message(NULL);
                            ?></center>
                    </div>
                <?php } ?>
            </td>
        </tr>
    </table>

    <div class="row">
        <div class="main col-md-7 col-sm-6 col-xs-12 col-sm-offset-3">
            <div class="head">
                <h1>Trainer Register</h1>
            </div>
            <form action="registration.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="username">Name</label>
                    <input type="text" name="name" placeholder="Full Name" class="form-username form-control" id="form-username">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" placeholder="Your Email" class="form-email form-control" id="form-email">
                </div>
                <div class="form-group">
                    <label for="text">Developer</label>
                    <input type="radio" name="developer" value="Web Designer"> Web Designer
                    <input type="radio" name="developer" value="C Programmer"> C Programmer
                    <input type="radio" name="developer" value="Web Developer"> Web Developer
                    <input type="radio" name="developer" value="JavaScript Developer"> JavaScript Developer
                </div>
                <div class="form-group">
                    <label for="photos">Profile Picture</label>
                    <input type="file" name="profile_picture" placeholder="" class="form-photo form-control" id="form-photo">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" placeholder="Your password" class="form-password form-control" id="form-password">
                </div>
                <button type="submit" name="register" class="btn btn-primary" style="background: rgba(48,219,18,0.5)">Sign Up</button>

            </form>
        </div>
    </div>
</div>
<div class="footer">
    <div class="part">
        <div class="part-1">
            <img src="../../../resource/image/logo/vc.png">
        </div>
        <div class="part-2">
            <div class="contact-us">
                <h1>Contact Us</h1>
                <div class="info">
                    <p>BSPI Computer Engineering Project.</p>
                    <p>7th Semister, 2nd Shift.</p>
                    <p>Computer Department,</p>
                    <p>Bangladesh Sweden Polytechnic Institute.</p>
                    <p>Tel : 01752-111111, 01631-222222</p>
                    <p><a href="">https://virtualclass-new.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="part-3">
        <div class="social">
            <ul>
                <li><a href=""><i class="icon icon-facebook"></i></a></li>
                <li><a href=""><i class="icon icon-twitter"></i></a></li>
                <li><a href=""><i class="icon icon-instagram"></i></a></li>
                <li><a href=""><i class="icon icon-youtube"></i></a></li>
                <li><a href=""><i class="icon icon-google-plus3"></i></a></li>
            </ul>
        </div>
    </div>
</div>

<script src="../../../resource/js/jquery-1.11.1.min.js"></script>

<script src="../../../resource/js/jquery.backstretch.min.js"></script>
<script src="../../../resource/js/scripts.js"></script>
<script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 10]>
<script src="../../../resource/js/placeholder.js"></script>
<![endif]-->

</body>

<script>
    $('.alert').slideDown("slow").delay(5000).slideUp("slow");
</script>
</html>