<?php
include_once ("../../../vendor/autoload.php");
if (!isset($_SESSION)) session_start();

use App\TrainerPanel\TrainerPanel;
use App\Message\Message;
use App\Utility\Utility;

$trainer = new TrainerPanel();

$trainer->setData($_GET);

$singleuser= $trainer->view();

if ($singleuser->email_verified == $_GET['email_token']){
    $trainer ->setData($_GET)->validTokenUpdate();
}
else{
    if ($singleuser->email_verified == 'Yes'){
        Message::message("
             <div class=\"alert alert-info\">
             <strong>Don't worry! </strong>This email already verified. Please login!
              </div>");
        Utility::redirect("log_in.php");
    }
    else{
        Message::message("
             <div class=\"alert alert-info\">
             <strong>Sorry! </strong>This Token is Invalid. Please signup with a valid email!
              </div>");
        Utility::redirect("sign_up.php");
    }

}