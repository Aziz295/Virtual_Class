<?php

include_once ("../../vendor/autoload.php");
if (!isset($_SESSION)) session_start();

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Trainer Panel</title>
    <link href="../../resource/css/style.css" rel="stylesheet">
    <link href="../../resource/css/trainerpanel.css" rel="stylesheet">
    <link href="../../resource/fonts.css" rel="stylesheet">
    <link href="../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="contain">
        <div class="header">
            <a href="#" id="logo"></a>
            <nav>
                <label for="drop" class="toggle">Menu</label>
                <input type="checkbox" id="drop" />
                <ul class="menu">
                    <li><a href="../../index.php">Home</a></li>
                    <li><a href="index.php">Traineer Panel</a></li>
                    <li><a href="../MyAccount/index.php">My Account</a></li>
                    <li>
                        <!-- First Tier Drop Down -->
                        <label for="drop-1" class="toggle-1">Sign Up +</label>
                        <a href="" class="drp">Sign Up</a>
                        <input type="checkbox" id="drop-1"/>
                        <ul>
                            <li><a href="" target="_blank">Trainer</a></li>
                            <li><a href="" target="_blank">Student</a></li>

                        </ul>

                    </li>
                    <li>
                        <!-- First Tier Drop Down -->
                        <label for="drop-2" class="toggle-2">Sign In +</label>
                        <a href="">Sign In</a>
                        <input type="checkbox" id="drop-2"/>
                        <ul>
                            <li><a href="" target="_blank">Admin</a></li>
                            <li><a href="" target="_blank">Trainer</a></li>
                            <li><a href="" target="_blank">Student</a></li>
                        </ul>

                    </li>
                </ul>
            </nav>
        </div>
        <div class="content">
            <div class="top-side">
                <div class="logo">
                    <img src="../../resource/image/logo/logoo.png">
                </div>
                <div class="category">
                    <nav class="nav">
                        <ul class="nav__menu">
                            <li class="nav__menu-item"><a>Course Category</a>
                                <ul class="nav__submenu">
                                    <li class="nav__submenu-item"> <a href="">Programming Language C</a></li>
                                    <li class="nav__submenu-item"> <a href="">Web Design Html & Css</a></li>
                                    <li class="nav__submenu-item"> <a href="">Web Development PHP</a></li>
                                    <li class="nav__submenu-item"> <a href="">JavaScript</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="search">
                    <input type="search" placeholder="Enter Your Keyword" name="search-here" class="search-box">
                    <button type="submit" name="submit" class="submit">Search</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="heading col-md-6 col-md-offset-5 col-sm-offset-4">
                <h1>Trainer Panel</h1>
            </div>
            <div class="popular">
                <div class="col-xs-12">
                    <h1>Most Popular</h1>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="" class="thumbnail">
                    <img src="../../resource/image/Trainer/Delowar.jpg" class="img-responsive">
                    <p>MD Delowar</p>
                    <p>JavaScript Developer</p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="" class="thumbnail">
                        <img src="../../resource/image/Trainer/piyal.jpg" class="img-responsive">
                        <p>Piyal Barua</p>
                        <p>Web Designer</p>
                    </a>
                </div>
            </div>
            <div class="trainer">
                <div class="col-xs-12">
                    <h1>Trainer</h1>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="" class="thumbnail">
                        <img src="../../resource/image/Trainer/shanto.jpg" class="img-responsive">
                        <p>Shirajul Islam Shanto</p>
                        <p>Co-Ordinator</p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="" class="thumbnail">
                        <img src="../../resource/image/Trainer/bappa.jpg" class="img-responsive">
                        <p>Nobi Hossain Bappa</p>
                        <p>Web Developer</p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="" class="thumbnail">
                        <img src="../../resource/image/Trainer/sukkur.jpg" class="img-responsive">
                        <p>Md Sukkur Ali(Mamun)</p>
                        <p>Python Programmer</p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="" class="thumbnail">
                        <img src="../../resource/image/Trainer/piyal.jpg" class="img-responsive">
                        <p>Piyal Barua</p>
                        <p>Web Designer</p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="" class="thumbnail">
                        <img src="../../resource/image/Trainer/Delowar.jpg" class="img-responsive">
                        <p>MD Delowar</p>
                        <p>JavaScript Developer</p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="" class="thumbnail">
                        <img src="../../resource/image/Trainer/Tareq.jpg" class="img-responsive">
                        <p>Tareq Aziz</p>
                        <p>Co-Ordinator</p>
                    </a>
                </div>
            </div>
            </div>
        </div>
<div class="abc">
    <a href="Authentication/logout.php">Log Out</a>
</div>
<div class="footer">
    <div class="part">
        <div class="part-1">
            <img src="../../resource/image/logo/vc.png">
        </div>
        <div class="part-2">
            <div class="contact-us">
                <h1>Contact Us</h1>
                <div class="info">
                    <p>BSPI Computer Engineering Project.</p>
                    <p>7th Semister, 2nd Shift.</p>
                    <p>Computer Department,</p>
                    <p>Bangladesh Sweden Polytechnic Institute.</p>
                    <p>Tel : 01752-111111, 01631-222222</p>
                    <p><a href="">https://virtualclass-new.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="part-3">
        <div class="social">
            <ul>
                <li><a href=""><i class="icon icon-facebook"></i></a></li>
                <li><a href=""><i class="icon icon-twitter"></i></a></li>
                <li><a href=""><i class="icon icon-instagram"></i></a></li>
                <li><a href=""><i class="icon icon-youtube"></i></a></li>
                <li><a href=""><i class="icon icon-google-plus3"></i></a></li>
            </ul>
        </div>
    </div>
</div>
</body>
</html>