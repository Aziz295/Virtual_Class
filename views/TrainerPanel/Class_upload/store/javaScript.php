<?php
include_once("../../../../vendor/autoload.php");

if (!isset($_SESSION)) session_start();

use App\TrainerPanel\ClassUpload;
use App\Message\Message;
use App\Utility\Utility;

$obj = new ClassUpload();
$file_name= time().$_FILES['video']['name'];
$file_tmp= $_FILES['video']['tmp_name'];
$destination="../../../../resource/class_video/javascript/".$file_name;
move_uploaded_file($file_tmp,$destination);
$_POST['video']=$file_name;

$obj ->setData($_POST);

$dd=$obj->javascript_class_store();

if ($dd){
    Message::setMessage("<div class='alert alert-danger'>
    <strong>Success!!</strong> Your class has been uploaded.. </div>");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}else{
    Message::setMessage("<div class='alert alert-danger'>
    <strong>Failed!</strong> Its not uploaded. </div>");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}