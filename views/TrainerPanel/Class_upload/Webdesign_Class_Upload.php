<?php
include_once ("../../../vendor/autoload.php");

if (!isset($_SESSION)) session_start();

use App\Message\Message;

?>

<!DOCTYPE html>
<html>

<head>
    <title>Class Upload</title>
    <meta charset="UTF-8">
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../resource/css/style.css" rel="stylesheet">
    <link href="../../../resource/css/Class_upload.css" rel="stylesheet">
    <link href="../../../resource/fonts.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<div class="contain">
    <div class="header">
        <a href="#" id="logo"></a>
        <nav>
            <label for="drop" class="toggle">Menu</label>
            <input type="checkbox" id="drop" />
            <ul class="menu">
                <li><a href="../../../index.php">Home</a></li>
                <li><a href="../../../index.php">Traineer Panel</a></li>
                <li><a href="../../MyAccount/index.php">My Account</a></li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-1" class="toggle-1">Sign Up +</label>
                    <a href="" class="drp">Sign Up</a>
                    <input type="checkbox" id="drop-1"/>
                    <ul>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>

                    </ul>

                </li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-2" class="toggle-2">Sign In +</label>
                    <a href="">Sign In</a>
                    <input type="checkbox" id="drop-2"/>
                    <ul>
                        <li><a href="" target="_blank">Admin</a></li>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>
                    </ul>

                </li>
            </ul>
        </nav>
    </div>
</div>
<div class="container">
    <div class="row">

        <table>
            <tr>
                <td width='230' >

                <td width='600' height="50" >


                    <?php  if(isset($_SESSION['message']) )if($_SESSION['message']!=""){ ?>

                        <div  id="message" class="form button"   style="font-size: smaller  " >
                            <center>
                                <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                                    echo "&nbsp;".Message::message();
                                }
                                Message::message(NULL);
                                ?></center>
                        </div>
                    <?php } ?>
                </td>
            </tr>
        </table>


        <div class="main">
            <div class="items col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                <div class="top">
                    <h2>Upload Your Class</h2>
                </div>
                <form action="store/webdesign.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="text">Class No :</label>
                        <input type="text" name="class_no" placeholder="Class Number" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="text">Class Topic</label>
                        <input type="text" name="topic" placeholder="Class Topic" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="text">Introduction</label>
                        <textarea typeof="text" name="introduction" placeholder="Class Introduction" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="video">Class Video</label>
                        <input type="file" name="video" class="form-control">
                    </div>
                    <button type="submit" name="submit" class="button-up">Upload</button>

                </form>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="part">
        <div class="part-1">
            <img src="../../../resource/image/logo/vc.png">
        </div>
        <div class="part-2">
            <div class="contact-us">
                <h1>Contact Us</h1>
                <div class="info">
                    <p>BSPI Computer Engineering Project.</p>
                    <p>7th Semister, 2nd Shift.</p>
                    <p>Computer Department,</p>
                    <p>Bangladesh Sweden Polytechnic Institute.</p>
                    <p>Tel : 01752-111111, 01631-222222</p>
                    <p><a href="">https://virtualclass-new.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="part-3">
        <div class="social">
            <ul>
                <li><a href=""><i class="icon icon-facebook"></i></a></li>
                <li><a href=""><i class="icon icon-twitter"></i></a></li>
                <li><a href=""><i class="icon icon-instagram"></i></a></li>
                <li><a href=""><i class="icon icon-youtube"></i></a></li>
                <li><a href=""><i class="icon icon-google-plus3"></i></a></li>
            </ul>
        </div>
    </div>
</div>

<script src="../../../resource/js/jquery-1.11.1.min.js"></script>

<script src="../../../resource/js/jquery.backstretch.min.js"></script>
<script src="../../../resource/js/scripts.js"></script>
<script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 10]>
<script src="../../../resource/js/placeholder.js"></script>
<![endif]-->

</body>

<script>
    $('.alert').slideDown("slow").delay(5000).slideUp("slow");
</script>
</body>

</html>