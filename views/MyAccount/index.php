<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>My Account</title>
    <link href="../../resource/css/MyAccount.css" rel="stylesheet">
    <link href="../../resource/css/style.css" rel="stylesheet">
    <link href="../../resource/fonts.css" rel="stylesheet">
    <link href="../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


</head>

<body>
<div class="contain">
    <div class="header">
        <a href="#" id="logo"></a>
        <nav>
            <label for="drop" class="toggle">Menu</label>
            <input type="checkbox" id="drop" />
            <ul class="menu">
                <li><a href="../../index.php">Home</a></li>
                <li><a href="../TrainerPanel/index.php">Traineer Panel</a></li>
                <li><a href="index.php">My Account</a></li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-1" class="toggle-1">Sign Up +</label>
                    <a href="#" class="drp">Sign Up</a>
                    <input type="checkbox" id="drop-1"/>
                    <ul>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>

                    </ul>

                </li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-2" class="toggle-2">Sign In +</label>
                    <a href="#">Sign In</a>
                    <input type="checkbox" id="drop-2"/>
                    <ul>
                        <li><a href="" target="_blank">Admin</a></li>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>
                    </ul>

                </li>
            </ul>
        </nav>
    </div>
    <div class="content">
        <div class="top-side">
            <div class="logo">
                <img src="../../resource/image/logo/logoo.png">
            </div>
            <div class="category">
                <nav class="nav">
                    <ul class="nav__menu">
                        <li class="nav__menu-item"><a>Course Category</a>
                            <ul class="nav__submenu">
                                <li class="nav__submenu-item"> <a href="">Programming Language C</a></li>
                                <li class="nav__submenu-item"> <a href="">Web Design Html & Css</a></li>
                                <li class="nav__submenu-item"> <a href="">Web Development PHP</a></li>
                                <li class="nav__submenu-item"> <a href="">JavaScript</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="search">
                <input type="search" placeholder="Enter Your Keyword" name="search-here" class="search-box">
                <button type="submit" name="submit" class="submit">Search</button>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="tp1 col-xs-12">
            <h1>My Account</h1>
        </div>
        <div class="main col-lg-12">
            <div class="account col-md-8 col-xs-12">
                <h1>Account</h1>
                <div class="acc col-md-5 col-sm-5 col-xs-12 col-lg-4">
                    <ul>
                        <li><a href="account_info.php"><i class="icon icon-profile"></i></a></li>
                    </ul>
                    <div class="acc-caption">
                        <h2>Account Information</h2>
                    </div>
                </div>
                <div class="acc col-md-5 col-sm-5 col-xs-12 col-lg-4">
                    <ul>
                        <li><a href=""><i class="icon icon-book"></i></a></li>
                    </ul>
                    <div class="acc-caption">
                        <h2>Address Book</h2>
                    </div>
                </div>
            </div>
            <div class="course col-md-8 col-lg-8">
                <h1>Course</h1>
                <div class="acc col-md-5 col-sm-5 col-xs-12 col-lg-4">
                    <ul>
                        <li><a href="my_order.php"><i class="icon icon-list"></i></a></li>
                    </ul>
                    <div class="acc-caption">
                        <h2>Order a Course</h2>
                    </div>
                </div>
                <div class="acc col-md-5 col-sm-5 col-xs-12 col-lg-4">
                    <ul>
                        <li><a href=""><i class="icon icon-indent-increase"></i></a></li>
                    </ul>
                    <div class="acc-caption">
                        <h2>My Course</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="part">
        <div class="part-1">
            <img src="../../resource/image/logo/vc.png">
        </div>
        <div class="part-2">
            <div class="contact-us">
                <h1>Contact Us</h1>
                <div class="info">
                    <p>BSPI Computer Engineering Project.</p>
                    <p>7th Semister, 2nd Shift.</p>
                    <p>Computer Department,</p>
                    <p>Bangladesh Sweden Polytechnic Institute.</p>
                    <p>Tel : 01752-111111, 01631-222222</p>
                    <p><a href="">https://virtualclass-new.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="part-3">
        <div class="social">
            <ul>
                <li><a href=""><i class="icon icon-facebook"></i></a></li>
                <li><a href=""><i class="icon icon-twitter"></i></a></li>
                <li><a href=""><i class="icon icon-instagram"></i></a></li>
                <li><a href=""><i class="icon icon-youtube"></i></a></li>
                <li><a href=""><i class="icon icon-google-plus3"></i></a></li>
            </ul>
        </div>
    </div>
</div>

</body>
</html>