<?php
include_once ("../../vendor/autoload.php");

if (!isset($_SESSION)) session_start();

use App\User\Select_Course;
use App\User\Auth;

$auth = new Auth();

$obj = new Select_Course();

$obj->setData();
$course= $obj->index();


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>My Course</title>
    <link href="../../resource/css/style.css" rel="stylesheet">
    <link href="../../resource/fonts.css" rel="stylesheet">
    <link href="../../resource/css/MyCourse.css" rel="stylesheet">
    <link href="../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<?php

$id = 0;
foreach ($course as $name){
    $id++;
}

?>

<div class="contain">
    <div class="header">
        <a href="#" id="logo"></a>
        <nav>
            <label for="drop" class="toggle">Menu</label>
            <input type="checkbox" id="drop" />
            <ul class="menu">
                <li><a href="../../index.php">Home</a></li>
                <li><a href="../TrainerPanel/index.php">Traineer Panel</a></li>
                <li><a href="../MyAccount/index.php">My Account</a></li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-1" class="toggle-1">Sign Up +</label>
                    <a href="#" class="drp">Sign Up</a>
                    <input type="checkbox" id="drop-1"/>
                    <ul>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>

                    </ul>

                </li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-2" class="toggle-2">Sign In +</label>
                    <a href="#">Sign In</a>
                    <input type="checkbox" id="drop-2"/>
                    <ul>
                        <li><a href="" target="_blank">Admin</a></li>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>
                    </ul>

                </li>
            </ul>
        </nav>
    </div>
    <div class="content">
        <div class="top-side">
            <div class="logo">
                <img src="../../resource/image/logo/logoo.png">
            </div>
            <div class="category">
                <nav class="nav">
                    <ul class="nav__menu">
                        <li class="nav__menu-item"><a>Course Category</a>
                            <ul class="nav__submenu">
                                <li class="nav__submenu-item"> <a href="">Programming Language C</a></li>
                                <li class="nav__submenu-item"> <a href="">Web Design Html & Css</a></li>
                                <li class="nav__submenu-item"> <a href="">Web Development PHP</a></li>
                                <li class="nav__submenu-item"> <a href="">JavaScript</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="search">
                <input type="search" placeholder="Enter Your Keyword" name="search-here" class="search-box">
                <button type="submit" name="submit" class="submit">Search</button>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="main">
            <div class="sidebar col-md-4">
                <h1>Account Dashboard</h1>
                <nav>
                    <ul>
                        <li><a href="">Account Information</a></li>
                        <li><a href="">Address Book</a></li>
                        <li><a href="">My Order</a></li>
                        <li><a href="">My Course</a></li>
                    </ul>
                </nav>
            </div>
            <div class="view col-md-8">
                <h1>Course Enrollment</h1>
                <table>
                    <th>Course Id</th>
                    <th>Course Name</th>
                    <th></th>
                    <tr>
                        <td><?php echo $name['id']?></td>
                        <td class="course-name"><?php echo $name['course_name']?></td>
                        <td><a href="../MyCource/<?php echo $name['course_name']?>/index.php">view course</a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="part">
        <div class="part-1">
            <img src="../../resource/image/logo/vc.png">
        </div>
        <div class="part-2">
            <div class="contact-us">
                <h1>Contact Us</h1>
                <div class="info">
                    <p>BSPI Computer Engineering Project.</p>
                    <p>7th Semister, 2nd Shift.</p>
                    <p>Computer Department,</p>
                    <p>Bangladesh Sweden Polytechnic Institute.</p>
                    <p>Tel : 01752-111111, 01631-222222</p>
                    <p><a href="">https://virtualclass-new.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="part-3">
        <div class="social">
            <ul>
                <li><a href=""><i class="icon icon-facebook"></i></a></li>
                <li><a href=""><i class="icon icon-twitter"></i></a></li>
                <li><a href=""><i class="icon icon-instagram"></i></a></li>
                <li><a href=""><i class="icon icon-youtube"></i></a></li>
                <li><a href=""><i class="icon icon-google-plus3"></i></a></li>
            </ul>
        </div>
    </div>
</div>

</body>
</html>