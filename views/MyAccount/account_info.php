<?php

include_once ("../../vendor/autoload.php");

if (!isset($_SESSION)) session_start();

use App\User\Student;
use App\User\Auth;

$auth = new Auth();

$obj = new Student();

$info = $obj->setData()->index();

?>

<!DOCTYPE html>
<html>
<head>
    <title>Account Information</title>
    <link  href="../../resource/css/acc_info.css" rel="stylesheet">
    <link href="../../resource/css/style.css" rel="stylesheet">
    <link href="../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../resource/fonts.css" rel="stylesheet">
</head>
<body>
<?php

$id = 0;
foreach ($info as $acc_item){
    $id++;
}

?>
<div class="contain">
    <div class="header">
        <a href="#" id="logo"></a>
        <nav>
            <label for="drop" class="toggle">Menu</label>
            <input type="checkbox" id="drop" />
            <ul class="menu">
                <li><a href="../../index.php">Home</a></li>
                <li><a href="../TrainerPanel/index.php">Traineer Panel</a></li>
                <li><a href="index.php">My Account</a></li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-1" class="toggle-1">Sign Up +</label>
                    <a href="#" class="drp">Sign Up</a>
                    <input type="checkbox" id="drop-1"/>
                    <ul>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>

                    </ul>

                </li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-2" class="toggle-2">Sign In +</label>
                    <a href="#">Sign In</a>
                    <input type="checkbox" id="drop-2"/>
                    <ul>
                        <li><a href="" target="_blank">Admin</a></li>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>
                    </ul>

                </li>
            </ul>
        </nav>
    </div>
    <div class="content">
        <div class="top-side">
            <div class="logo">
                <img src="../../resource/image/logo/logoo.png">
            </div>
            <div class="category">
                <nav class="nav">
                    <ul class="nav__menu">
                        <li class="nav__menu-item"><a>Course Category</a>
                            <ul class="nav__submenu">
                                <li class="nav__submenu-item"> <a href="">Programming Language C</a></li>
                                <li class="nav__submenu-item"> <a href="">Web Design Html & Css</a></li>
                                <li class="nav__submenu-item"> <a href="">Web Development PHP</a></li>
                                <li class="nav__submenu-item"> <a href="">JavaScript</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="search">
                <input type="search" placeholder="Enter Your Keyword" name="search-here" class="search-box">
                <button type="submit" name="submit" class="submit">Search</button>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="main">
            <div class="sidebar col-md-3 col-sm-3 col-xs-12">
                <h2>Account Dashboard</h2>
                <nav>
                    <ul>
                        <li><a href="">Account Information</a></li>
                        <li><a href="">Address Book</a></li>
                        <li><a href="">My Order</a></li>
                        <li><a href="">My Course</a></li>
                    </ul>
                </nav>
            </div>
            <div class="view col-md-8 col-sm-9 col-xs-12">
                <div class="heading">
                    <h2>Account Information</h2>
                    <p>Look at your information which you give us.. You can change anything of your Profile Items</p>
                </div>
                <div class="account">
                    <div class="profile-head col-md-5 col-sm-4 col-xs-12">
                        <a href="" class="thumbnail">
                            <img src="<?php echo "../../resource/Uploads/".$acc_item['image']?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="profile-info">
                        <table class="col-md-8 col-sm-12 col-xs-12">
                            <tr class="">
                                <td class="">First Name : </td>
                                <td><?php echo $acc_item['first_name']?></td>
                            </tr>
                            <tr>
                                <td>Last Name : </td>
                                <td><?php echo $acc_item['last_name']?></td>
                            </tr>
                            <tr>
                                <td>Username : </td>
                                <td><?php echo $acc_item['username']?></td>
                            </tr>
                            <tr>
                                <td>Email : </td>
                                <td><?php  echo $acc_item['email']?></td>
                            </tr>
                            <tr>
                                <td>Profession : </td>
                                <td><?php echo $acc_item['profession']?></td>
                            </tr>
                            <tr>
                                <td>Gender : </td>
                                <td><?php echo $acc_item['gender']?></td>
                            </tr>
                        </table>
                        <div class="info-p col-md-6">
                            <a href="">Change Password??</a><br/>
                            <a href="">Edit Your Profile??</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="part">
        <div class="part-1">
            <img src="../../resource/image/logo/vc.png">
        </div>
        <div class="part-2">
            <div class="contact-us">
                <h1>Contact Us</h1>
                <div class="info">
                    <p>BSPI Computer Engineering Project.</p>
                    <p>7th Semister, 2nd Shift.</p>
                    <p>Computer Department,</p>
                    <p>Bangladesh Sweden Polytechnic Institute.</p>
                    <p>Tel : 01752-111111, 01631-222222</p>
                    <p><a href="">https://virtualclass-new.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="part-3">
        <div class="social">
            <ul>
                <li><a href=""><i class="icon icon-facebook"></i></a></li>
                <li><a href=""><i class="icon icon-twitter"></i></a></li>
                <li><a href=""><i class="icon icon-instagram"></i></a></li>
                <li><a href=""><i class="icon icon-youtube"></i></a></li>
                <li><a href=""><i class="icon icon-google-plus3"></i></a></li>
            </ul>
        </div>
    </div>
</div>

</body>
</html>