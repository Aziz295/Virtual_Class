<?php
include_once ("../../vendor/autoload.php");

if (!isset($_SESSION)) session_start();
use App\Message\Message;

?>
<!DOCTYPE html>
<html>
<head>
    <title>My Order</title>
    <link href="../../resource/css/style.css" rel="stylesheet">
    <link href="../../resource/css/My_order.css" rel="stylesheet">
    <link href="../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../resource/fonts.css" rel="stylesheet">
</head>
<body>
<div class="contain">
    <div class="header">
        <a href="#" id="logo"></a>
        <nav>
            <label for="drop" class="toggle">Menu</label>
            <input type="checkbox" id="drop" />
            <ul class="menu">
                <li><a href="../../index.php">Home</a></li>
                <li><a href="../TrainerPanel/index.php">Traineer Panel</a></li>
                <li><a href="../MyAccount/index.php">My Account</a></li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-1" class="toggle-1">Sign Up +</label>
                    <a href="#" class="drp">Sign Up</a>
                    <input type="checkbox" id="drop-1"/>
                    <ul>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>

                    </ul>

                </li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-2" class="toggle-2">Sign In +</label>
                    <a href="#">Sign In</a>
                    <input type="checkbox" id="drop-2"/>
                    <ul>
                        <li><a href="" target="_blank">Admin</a></li>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>
                    </ul>

                </li>
            </ul>
        </nav>
    </div>
    <div class="content">
        <div class="top-side">
            <div class="logo">
                <img src="../../resource/image/logo/logoo.png">
            </div>
            <div class="category">
                <nav class="nav">
                    <ul class="nav__menu">
                        <li class="nav__menu-item"><a>Course Category</a>
                            <ul class="nav__submenu">
                                <li class="nav__submenu-item"> <a href="">Programming Language C</a></li>
                                <li class="nav__submenu-item"> <a href="">Web Design Html & Css</a></li>
                                <li class="nav__submenu-item"> <a href="">Web Development PHP</a></li>
                                <li class="nav__submenu-item"> <a href="">JavaScript</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="search">
                <input type="search" placeholder="Enter Your Keyword" name="search-here" class="search-box">
                <button type="submit" name="submit" class="submit">Search</button>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">

        <table>
            <tr>
                <td width='230' >

                <td width='600' height="50" >


                    <?php  if(isset($_SESSION['message']) )if($_SESSION['message']!=""){ ?>

                        <div  id="message" class="form button"   style="font-size: smaller  " >
                            <center>
                                <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                                    echo "&nbsp;".Message::message();
                                }
                                Message::message(NULL);
                                ?></center>
                        </div>
                    <?php } ?>
                </td>
            </tr>
        </table>
        <div class="main">
            <div class="sidebar col-md-4 col-sm-4">
                <h2>Account Dashboard</h2>
                <nav>
                    <ul>
                        <li><a href="">Account Information</a></li>
                        <li><a href="">Address Book</a></li>
                        <li><a href="">My Order</a></li>
                        <li><a href="">My Course</a></li>
                    </ul>
                </nav>
            </div>
            <div class="view col-md-8 col-sm-8">

                <form action="store/course_store.php" method="post" class="form-l col-md-6 col-md-offset-4 col-sm-8 col-sm-offset-3">
                    <div class="headding">
                        <h2>Select Your Course</h2>
                    </div>
                    <input type="hidden" name="student_id" value="<?php $_SESSION['student_id']?>">
                    <input type="hidden" name="email" value="<?php $_SESSION['email']?>">
                    <div class="checkbox" class="check">
                        <label><input type="checkbox" name="course_name[]" value="Web Design" > Web Design</label><br>
                        <label><input type="checkbox" name="course_name[]" value="Web Development" > Web Development</label><br>
                        <label><input type="checkbox" name="course_name[]" value="JavaScript" > JavaScript</label><br>
                        <label><input type="checkbox" name="course_name[]" value="C Programming" > C Programming</label>
                    </div>
                    <button type="submit" class="button-o">Add Your Course</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="part">
        <div class="part-1">
            <img src="../../resource/image/logo/vc.png">
        </div>
        <div class="part-2">
            <div class="contact-us">
                <h1>Contact Us</h1>
                <div class="info">
                    <p>BSPI Computer Engineering Project.</p>
                    <p>7th Semister, 2nd Shift.</p>
                    <p>Computer Department,</p>
                    <p>Bangladesh Sweden Polytechnic Institute.</p>
                    <p>Tel : 01752-111111, 01631-222222</p>
                    <p><a href="">https://virtualclass-new.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="part-3">
        <div class="social">
            <ul>
                <li><a href=""><i class="icon icon-facebook"></i></a></li>
                <li><a href=""><i class="icon icon-twitter"></i></a></li>
                <li><a href=""><i class="icon icon-instagram"></i></a></li>
                <li><a href=""><i class="icon icon-youtube"></i></a></li>
                <li><a href=""><i class="icon icon-google-plus3"></i></a></li>
            </ul>
        </div>
    </div>
</div>

<script src="../../resource/js/jquery-1.11.1.min.js"></script>

<script src="../../resource/js/jquery.backstretch.min.js"></script>
<script src="../../resource/js/scripts.js"></script>
<script src="../../resource/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 10]>
<script src="../../resource/js/placeholder.js"></script>
<![endif]-->

</body>

<script>
    $('.alert').slideDown("slow").delay(5000).slideUp("slow");
</script>

</html>
