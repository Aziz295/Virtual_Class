<?php
include_once ("../../../vendor/autoload.php");
if (!isset($_SESSION)) session_start();

use App\User\Student;
use App\Message\Message;
use App\Utility\Utility;

$student = new Student();

$student->setData($_GET);

$singleuser= $student->view();

if ($singleuser->email_verified == $_GET['email_token']){
    $student ->setData($_GET)->validTokenUpdate();
}
else{
    if ($singleuser->email_verified == 'Yes'){
        Message::message("
             <div class=\"alert alert-info\">
             <strong>Don't worry! </strong>This email already verified. Please login!
              </div>");
        Utility::redirect("log_in.php");
    }
    else{
        Message::message("
             <div class=\"alert alert-info\">
             <strong>Sorry! </strong>This Token is Invalid. Please signup with a valid email!
              </div>");
        Utility::redirect("sign_up.php");
    }

}