<?php

include_once ("../../../vendor/autoload.php");
if (!isset($_SESSION)) session_start();

use App\Message\Message;

?>

<!DOCTYPE html>
<html>
<head>
    <title>User Registration</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../../../resource/css/style.css" rel="stylesheet">
    <link href="../../../resource/fonts.css" rel="stylesheet">
    <link href="../../../resource/css/user_registration.css" rel="stylesheet">
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="contain">
    <div class="header">
        <a href="#" id="logo"></a>
        <nav>
            <label for="drop" class="toggle">Menu</label>
            <input type="checkbox" id="drop" />
            <ul class="menu">
                <li><a href="../../../index.php">Home</a></li>
                <li><a href="../../../index.php">Traineer Panel</a></li>
                <li><a href="../../MyAccount/index.php">My Account</a></li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-1" class="toggle-1">Sign Up +</label>
                    <a href="" class="drp">Sign Up</a>
                    <input type="checkbox" id="drop-1"/>
                    <ul>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>

                    </ul>

                </li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-2" class="toggle-2">Sign In +</label>
                    <a href="">Sign In</a>
                    <input type="checkbox" id="drop-2"/>
                    <ul>
                        <li><a href="" target="_blank">Admin</a></li>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>
                    </ul>

                </li>
            </ul>
        </nav>
    </div>
</div>
<div class="container">

    <table>
        <tr>
            <td width='230' >

            <td width='600' height="50" >


                <?php  if(isset($_SESSION['message']) )if($_SESSION['message']!=""){ ?>

                    <div  id="message" class="form button"   style="font-size: smaller  " >
                        <center>
                            <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                                echo "&nbsp;".Message::message();
                            }
                            Message::message(NULL);
                            ?></center>
                    </div>
                <?php } ?>
            </td>
        </tr>
    </table>
    <div class="row">
        <div class="main col-md-6 col-sm-6 col-xs-12 col-sm-offset-3">
            <div class="head">
                <h1>User Registration</h1>
            </div>
            <form action="registration.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input type="text" name="first_name" placeholder="First Name..." class="form-username form-control" id="form-username">
                </div>
                <div class="form-group">
                    <label for="last_name">Last Name</label>
                    <input type="text" name="last_name" placeholder="Last Name..." class="form-username form-control" id="form-username">
                </div>
                <div class="form-group">
                    <label for="username">User Name</label>
                    <input type="text" name="username" placeholder="username..." class="form-username form-control" id="form-username">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" placeholder="Your Email" class="form-email form-control" id="form-email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" placeholder="Your password" class="form-password form-control" id="form-password">
                </div>
                <div class="form-group">
                    <label for="phone_no">Phone No</label>
                    <input type="tel" name="phone_no" placeholder="Your Phone No" class="form-phone_no form-control" id="form-phone_no">
                </div>
                <div class="form-group">
                    <label for="present_add">Present Address</label>
                    <input type="text" name="present_add" placeholder="Present Address" class="form-present_add form-control" id="form-present_add">
                </div>
                <div class="form-group">
                    <label for="parmanent_add">Parmanent Address</label>
                    <input type="text" name="parmanent_add" placeholder="Your Parmanent Address" class="form-parmanent_add form-control" id="form-parmanent_add">
                </div>
                <div class="form-group">
                    <label for="profession">Profession</label>
                    <input type="text" name="profession" placeholder="Your Profession" class="form-profession form-control" id="form-profession">
                </div>
                <div class="form-group">
                    <label for="gender">Gender</label>
                    <input type="radio" name="gender" value="Male"> Male
                    <input type="radio" name="gender" value="Female"> Female
                </div>
                <div class="form-group">
                    <label for="nationality">Nationality</label>
                    <input type="text" name="nationality" placeholder="Your Nationality" class="form-nationality form-control" id="form-nationality">
                </div>
                <div class="form-group">
                    <label for="form-profile_picture">Image</label>
                    <input type="file" name="image" placeholder="choose image">

                </div>
                <button type="submit" name="register" class="btn bg-primary ">Register</button>
            </form>
        </div>

    </div>
</div>

<div class="footer">
    <div class="part">
        <div class="part-1">
            <img src="../../../resource/image/logo/vc.png">
        </div>
        <div class="part-2">
            <div class="contact-us">
                <h1>Contact Us</h1>
                <div class="info">
                    <p>BSPI Computer Engineering Project.</p>
                    <p>7th Semister, 2nd Shift.</p>
                    <p>Computer Department,</p>
                    <p>Bangladesh Sweden Polytechnic Institute.</p>
                    <p>Tel : 01752-111111, 01631-222222</p>
                    <p><a href="">https://virtualclass-new.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="part-3">
        <div class="social">
            <ul>
                <li><a href=""><i class="icon icon-facebook"></i></a></li>
                <li><a href=""><i class="icon icon-twitter"></i></a></li>
                <li><a href=""><i class="icon icon-instagram"></i></a></li>
                <li><a href=""><i class="icon icon-youtube"></i></a></li>
                <li><a href=""><i class="icon icon-google-plus3"></i></a></li>
            </ul>
        </div>
    </div>
</div>

<script src="../../../resource/js/jquery-1.11.1.min.js"></script>

<script src="../../../resource/js/jquery.backstretch.min.js"></script>
<script src="../../../resource/js/scripts.js"></script>
<script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 10]>
<script src="../../../resource/js/placeholder.js"></script>
<![endif]-->


</body>

<script>
    $('.alert').slideDown("slow").delay(5000).slideUp("slow");
</script>
</html>