<?php
if(!isset($_SESSION) )session_start();
include_once('../../../vendor/autoload.php');
use App\User\Student;
use App\Message\message;



if(isset($_POST['email'])) {
    $obj= new Student();
    $obj->setData($_POST);
    $singleUser = $obj->view();

    $passwordResetLink= $singleUser->password ;

    require '../../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug  = 0;
    $mail->SMTPAuth   = true;
    $mail->SMTPSecure = "ssl";
    $mail->Host       = "smtp.gmail.com";
    $mail->Port       = 465;
    $mail->AddAddress($_POST['email']);
    $mail->Username="ai3482507@gmail.com";
    $mail->Password="00112233";
    $mail->SetFrom(' ai3482507@gmail.com','Virtual Class');
    $mail->AddReplyTo("ai3482507@gmail.com","Virtual Class");
    $mail->Subject    = "Your Password Reset Link";
    $message =  "Please click this link to reset your password: 
       http://localhost/Virtual_Class/views/User/User_register/resetpassword.php?email=".$_POST['email']."&code=".$singleUser->password;
    $mail->MsgHTML($message);
    if($mail->Send()){

        Message::message("
                <div class=\"alert alert-success\">
                            <strong>Email Sent!</strong> Please check your email for password reset link.
                </div>");
    }

}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Forgotten Password</title>
    <link href="../../../resource/css/adminlogin.css" rel="stylesheet">
    <link href="../../../resource/css/style.css" rel="stylesheet">
    <link href="../../../resource/fonts.css" rel="stylesheet">
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
<div class="header">
    <a href="#" id="logo"></a>
    <nav>
        <label for="drop" class="toggle">Menu</label>
        <input type="checkbox" id="drop" />
        <ul class="menu">
            <li><a href="../../../index.php" >Home</a></li>
            <li><a href="../../../index.php">Traineer Panel</a></li>
            <li><a href="../../MyAccount/index.php">My Account</a></li>
            <li>
                <!-- First Tier Drop Down -->
                <label for="drop-1" class="toggle-1">Sign Up +</label>
                <a href="#" class="drp">Sign Up</a>
                <input type="checkbox" id="drop-1"/>
                <ul>
                    <li><a href="" target="_blank">Trainer</a></li>
                    <li><a href="" target="_blank">Student</a></li>

                </ul>

            </li>
            <li>
                <!-- First Tier Drop Down -->
                <label for="drop-2" class="toggle-2">Sign In +</label>
                <a href="#">Sign In</a>
                <input type="checkbox" id="drop-2"/>
                <ul>
                    <li><a href="" target="_blank">Admin</a></li>
                    <li><a href="" target="_blank">Trainer</a></li>
                    <li><a href="" target="_blank">Student</a></li>
                </ul>

            </li>
        </ul>
    </nav>
</div>
<div class="container">
    <div class="row">

        <table>
            <tr>
                <td width='230' >

                <td width='600' height="50" >


                    <?php  if(isset($_SESSION['message']) )if($_SESSION['message']!=""){ ?>

                        <div  id="message" class="form button"   style="font-size: smaller  " >
                            <center>
                                <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                                    echo "&nbsp;".Message::message();
                                }
                                Message::message(NULL);
                                ?></center>
                        </div>
                    <?php } ?>
                </td>
            </tr>
        </table>

        <div class="main col-md-4 col-sm-6 col-xs-12 col-sm-offset-4">
            <div class="head">
                <h1>Student Login</h1>
                <p>Enter Your Valid Mail To Get this link</p>
            </div>
            <form action="" method="post">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" placeholder="Your Email" class="form-email form-control" id="form-email">
                </div>
                <button type="submit" name="submit" class="btn btn-primary">Send</button>

            </form>
        </div>
    </div>
</div>
<div class="footer">
    <div class="part">
        <div class="part-1">
            <img src="../../../resource/image/logo/vc.png">
        </div>
        <div class="part-2">
            <div class="contact-us">
                <h1>Contact Us</h1>
                <div class="info">
                    <p>BSPI Computer Engineering Project.</p>
                    <p>7th Semister, 2nd Shift.</p>
                    <p>Computer Department,</p>
                    <p>Bangladesh Sweden Polytechnic Institute.</p>
                    <p>Tel : 01752-111111, 01631-222222</p>
                    <p><a href="">https://virtualclass-new.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="part-3">
        <div class="social">
            <ul>
                <li><a href=""><i class="icon icon-facebook"></i></a></li>
                <li><a href=""><i class="icon icon-twitter"></i></a></li>
                <li><a href=""><i class="icon icon-instagram"></i></a></li>
                <li><a href=""><i class="icon icon-youtube"></i></a></li>
                <li><a href=""><i class="icon icon-google-plus3"></i></a></li>
            </ul>
        </div>
    </div>
</div>
</body>
</html>