<?php
namespace App\User;

use App\Model\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class Student extends Database{
    public $id;
    public $first_name;
    public $last_name;
    public $username;
    public $email;
    public $password;
    public $phone_no;
    public $present_add;
    public $permanent_add;
    public $profession;
    public $gender;
    public $nationality;
    public $image;
    public $email_token;


    public function __construct()
    {
        parent::__construct();
    }


    public function setData($data=array()){
        if (array_key_exists('first_name', $data)){
            $this->first_name=$data['first_name'];
        }
        if (array_key_exists('last_name', $data)){
            $this->last_name=$data['last_name'];
        }
        if (array_key_exists('username', $data)){
            $this->username = $data['username'];
        }
        if (array_key_exists('email', $data)){
            $this->email=$data['email'];
        }
        if (array_key_exists('password', $data)){
            $this->password=md5($data['password']);
        }
        if (array_key_exists('phone_no', $data)){
            $this->phone_no=$data['phone_no'];
        }
        if (array_key_exists('present_add', $data)){
            $this->present_add=$data['present_add'];
        }
        if (array_key_exists('parmanent_add', $data)){
            $this->permanent_add=$data['parmanent_add'];
        }
        if (array_key_exists('profession', $data)){
            $this->profession=$data['profession'];
        }
        if (array_key_exists('gender', $data)){
            $this->gender=$data['gender'];
        }
        if (array_key_exists('nationality', $data)){
            $this->nationality=$data['nationality'];
        }
        if (array_key_exists('image', $data)){
            $this->image=$data['image'];
        }
        if (array_key_exists('email_token', $data)){
            $this->email_token=$data['email_token'];
        }
        return $this;

    }

    public function store(){
        $arrData = array($this->first_name,$this->last_name,$this->username,$this->email,$this->password,$this->phone_no,$this->present_add,$this->permanent_add,$this->profession,$this->gender,$this->nationality,$this->image,$this->email_token);
        $sql = "INSERT INTO `student`( `first_name`, `last_name`, `username`, `email`, `password`, `phone_no`, `present_add`, `permanent_add`, `profession`, `gender`, `nationality`, `image`, `email_verified`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> You Registered Successfully, Please check your email and active your account.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }

    }

    public function change_password(){
        $arrData= array($this->email,$this->password);
        $sql="UPDATE `student` SET `password`=:password  WHERE `email` =:email";
        $STH= $this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if ($result){
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Password has been updated  successfully.
              </div>");
        }
        else {
            echo "Error";
        }
    }

    public function index(){
        $arrData = array();
        $sql = "SELECT * FROM `student`";
        $STH  = $this->DBH->query($sql);
        while ($result = $STH->fetch(PDO::FETCH_ASSOC)){
            $arrData[]= $result;
        }
        return $arrData;
    }

    public function view(){
        $sql="SELECT * FROM `student` WHERE `email` ='$this->email'";

        $STH =$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();
    }

    public function validTokenUpdate(){
        $sql="UPDATE `student` SET  `email_verified`='".'Yes'."' WHERE `student`.`email` ='$this->email'";
        $result=$this->DBH->prepare($sql);
        $result->execute();

        if($result){
            Message::message("
             <div class=\"alert alert-success\">
             <strong>Success!</strong> Email verification has been successful. Please login now!
              </div>");
        }
        else {
            echo "Error";
        }
        return Utility::redirect('../../views/User/User_register/log_in.php');
    }


    public function update(){
        $arrData= array($this->username,$this->email,$this->password);
        $sql = "UPDATE `student` SET `username`=:username,`email`=:email,`password`=:password WHERE `email`=:email";
        $STH= $this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Data has been updated  successfully.
              </div>");
        }
        else {
            echo "Error";
        }
        return Utility::redirect($_SERVER['HTTP_REFERER']);

    }





}