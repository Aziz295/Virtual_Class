<?php
namespace App\User;

use App\Model\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class Select_Course extends Database{
    public $id;
    public $student_id;
    public $email;
    public $course_name;

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data= array()){
        if (array_key_exists('student_id', $data)){
            $this->student_id = $data['student_id'];
        }
        if (array_key_exists('email', $data)){
            $this->email = $data['email'];
        }
        if (array_key_exists('course_name', $data)){
            $array = $data['course_name'];
            $this->course_name = implode(',', $array);
        }
    }

    public function store(){
        $arrData = array($this->student_id, $this->email, $this->course_name);
        $sql = "INSERT INTO `student_course`(`student_id`, `email`, `course_name`) VALUES (?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> You are Successfully Add Your Course.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }

    }


    public function index(){
        $arrData = array();
        $sql = "SELECT * FROM `student_course`";
        $STH  = $this->DBH->query($sql);
        while ($result = $STH->fetch(PDO::FETCH_ASSOC)){
            $arrData[]= $result;
        }
        return $arrData;
    }

    public function view(){
        $sql="SELECT * FROM `student_course` WHERE `email` ='$this->email'";

        $STH =$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();
    }

}