<?php
namespace App\Admin;

use App\Model\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class Admin extends Database{
    public $id;
    public $username;
    public $email;
    public $password;
    public $email_token;

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data=array()){
        if (array_key_exists('username', $data)){
            $this->username=$data['username'];
        }
        if (array_key_exists('email', $data)){
            $this->email=$data['email'];
        }
        if (array_key_exists('password', $data)){
            $this->password=md5($data['password']);
        }
        if (array_key_exists('email_token', $data)){
            $this->email_token=$data['email_token'];
        }
        return $this;

    }

    public function store(){
        $arrData=array($this->username,$this->email,$this->password,$this->email_token);
        $sql= "INSERT INTO `admin`(`username`, `email`, `password`, `email_verified`) VALUES (?,?,?,?)";
        $STH= $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> You Registered Successfully, Please check your email and active your account.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }

    }

    public function change_password(){
        $arrData= array($this->username,$this->password);
        $sql="UPDATE `admin` SET `password`=:password  WHERE `email` =:email";
        $STH= $this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if ($result){
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Password has been updated  successfully.
              </div>");
        }
        else {
            echo "Error";
        }
    }

    public function view(){
        $sql="SELECT * FROM `admin` WHERE `email` ='$this->email'";

        $STH =$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();
    }

    public function validTokenUpdate(){
        $sql="UPDATE `admin` SET  `email_verified`='".'Yes'."' WHERE `admin`.`email` ='$this->email'";
        $result=$this->DBH->prepare($sql);
        $result->execute();

        if($result){
            Message::message("
             <div class=\"alert alert-success\">
             <strong>Success!</strong> Email verification has been successful. Please login now!
              </div>");
        }
        else {
            echo "Error";
        }
        return Utility::redirect('../../views/Admin/profile/sign_up.php');
    }

        public function update(){
            $arrData= array($this->username,$this->email,$this->password);
            $sql = "UPDATE `admin` SET `username`=:username,`email`=:email,`password`=:password WHERE `email`=:email";
            $STH= $this->DBH->prepare($sql);
            $result=$STH->execute($arrData);

            if($result){
                Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Data has been updated  successfully.
              </div>");
            }
            else {
                echo "Error";
            }
            return Utility::redirect($_SERVER['HTTP_REFERER']);

        }






}