<?php
namespace App\Comment;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database;
use PDO;

class Comment extends Database{

    public $id;
    public $username;
    public $profile_pic;
    public $class_no;
    public $comment;


    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data=array()){

        if (array_key_exists('username', $data)){
            $this->username = $data['username'];
        }
        if (array_key_exists('profile_pic', $data)){
            $this->profile_pic = $data['profile_pic'];
        }
        if (array_key_exists('class_no', $data)){
            $this->class_no = $data['class_no'];
        }
        if (array_key_exists('comment', $data)){
            $this->comment = $data['comment'];
        }
    }

    public function web_development_comment_store(){
        $arrdata = array($this->username,$this->profile_pic, $this->class_no, $this->comment);
        $sql = "INSERT INTO `web_development_comment`(`username`, `profile_pic`, `class_no`, `comment`) VALUES (?,?,?,?)";

        $STH  = $this->DBH->prepare($sql);

        $result = $STH->execute($arrdata);

        if ($result){
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Comment Uploaded..
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Failed to upload..
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }

    }

}