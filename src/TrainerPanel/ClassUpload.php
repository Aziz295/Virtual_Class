<?php
namespace App\TrainerPanel;

use App\Model\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class ClassUpload extends Database{
    public $id;
    public $class_no;
    public $topic;
    public $introduction;
    public $video;


    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data=array()){
        if (array_key_exists('class_no',$data)){
            $this->class_no= $data['class_no'];
        }
        if (array_key_exists('topic',$data)){
            $this->topic= $data['topic'];
        }
        if (array_key_exists('introduction',$data)){
            $this->introduction= $data['introduction'];
        }
        if (array_key_exists('video',$data)){
            $this->video= $data['video'];
        }
    }


    public function web_development_class_store(){
        $arrData =array($this->class_no,$this->topic,$this->introduction,$this->video);
        $sql = "INSERT INTO `web_development_class`(`class_no`, `topic`, `introduction`, `video`) VALUES (?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> You Inserted Successfully..
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been Inserted successfully.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function web_design_class_store(){
        $arrData =array($this->class_no,$this->topic,$this->introduction,$this->video);
        $sql = "INSERT INTO `web_design_class`(`class_no`, `topic`, `introduction`, `video`) VALUES (?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> You Inserted Successfully..
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been Inserted successfully.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function c_programming_class_store(){
        $arrData =array($this->class_no,$this->topic,$this->introduction,$this->video);
        $sql = "INSERT INTO `c_programming_class`(`class_no`, `topic`, `introduction`, `video`) VALUES (?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> You Inserted Successfully..
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been Inserted successfully.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }


    public function javascript_class_store(){
        $arrData =array($this->class_no,$this->topic,$this->introduction,$this->video);
        $sql = "INSERT INTO `javascript_class`(`class_no`, `topic`, `introduction`, `video`) VALUES (?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> You Inserted Successfully..
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been Inserted successfully.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }


    public function webdevelopment_index(){
        $arrData = array();
        $sql = "SELECT * FROM `web_development_class`";
        $STH  = $this->DBH->query($sql);
        while ($result = $STH->fetch(PDO::FETCH_ASSOC)){
            $arrData[]= $result;
        }
        return $arrData;

    }
    public function webdevelopment_pagination($page = 0, $itemPerPage=1){

        $start = (($page-1) * $itemPerPage);
        $sql = "SELECT * from `web_development_class` LIMIT $start,$itemPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }







}