<?php
namespace App\TrainerPanel;

use App\Model\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class TrainerPanel extends Database{

    public $id;
    public $name;
    public $email;
    public $developer;
    public $profile_picture;
    public $password;
    public $email_token;


    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data=array()){
        if (array_key_exists('name', $data)){
            $this->name=$data['name'];
        }
        if (array_key_exists('email', $data)){
            $this->email=$data['email'];
        }
        if (array_key_exists('developer', $data)){
            $this->developer=$data['developer'];
        }
        if (array_key_exists('profile_picture', $data)){
            $this->profile_picture=$data['profile_picture'];
        }
        if (array_key_exists('password', $data)){
            $this->password=md5($data['password']);
        }
        if (array_key_exists('email_token', $data)){
            $this->email_token=$data['email_token'];
        }
        return $this;

    }

    public function store(){
        $arrData=array($this->name,$this->email,$this->developer,$this->profile_picture,$this->password,$this->email_token);
        $sql= "INSERT INTO `trainerpanel`(`name`, `email`, `developer`, `profile_picture`, `password`, `email_verified`) VALUES (?,?,?,?,?,?)";
        $STH= $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> You Registered Successfully, Please check your email and active your account.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }

    }

    public function change_password(){
        $arrData= array($this->email,$this->password);
        $sql="UPDATE `trainerpanel` SET `password`=:password  WHERE `email` =:email";
        $STH= $this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if ($result){
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Password has been updated  successfully.
              </div>");
        }
        else {
            echo "Error";
        }
    }


    public function index(){
        $arrData = array();
        $sql = "SELECT * FROM `trainerpanel`";
        $STH  = $this->DBH->query($sql);
        while ($result = $STH->fetch(PDO::FETCH_ASSOC)){
            $arrData[]= $result;
        }
        return $arrData;
    }

    public function view(){
        $sql="SELECT * FROM `trainerpanel` WHERE `email` ='$this->email'";

        $STH =$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();
    }

    public function validTokenUpdate(){
        $sql="UPDATE `trainerpanel` SET  `email_verified`='".'Yes'."' WHERE `trainerpanel`.`email` ='$this->email'";
        $result=$this->DBH->prepare($sql);
        $result->execute();

        if($result){
            Message::message("
             <div class=\"alert alert-success\">
             <strong>Success!</strong> Email verification has been successful. Please login now!
              </div>");
        }
        else {
            echo "Error";
        }
        return Utility::redirect('../../views/TrainerPanel/profile/sign_up.php');
    }

    public function update(){
        $arrData= array($this->name,$this->email,$this->password);
        $sql = "UPDATE `trainerpanel` SET `name`=:name,`email`=:email,`password`=:password WHERE `email`=:email";
        $STH= $this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Data has been updated  successfully.
              </div>");
        }
        else {
            echo "Error";
        }
        return Utility::redirect($_SERVER['HTTP_REFERER']);

    }


}