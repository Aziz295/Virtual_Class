<?php
namespace App\Model;

use PDO;
use PDOException;
class Database{
    public $DBH;
    public $host="localhost";
    public $dbname="virtual_class";
    public $user="root";
    public $password="";



    public function __construct()
    {

        try {

            # MySQL with PDO_MYSQL
            $this-> DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user,$this->password);
            echo "connected Successfully";

        }
        catch(PDOException $e){
            echo $e->getMessage();
        }
    }
}
