<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Virtual Class</title>
    <link href="resource/css/style.css" rel="stylesheet">
    <link href="resource/fonts.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="container">
    <div class="header">
        <a href="#" id="logo"></a>
        <nav>
            <label for="drop" class="toggle">Menu</label>
            <input type="checkbox" id="drop" />
            <ul class="menu">
                <li><a href="#">Home</a></li>
                <li><a href="views/TrainerPanel/index.php">Traineer Panel</a></li>
                <li><a href="views/MyAccount/index.php">My Account</a></li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-1" class="toggle-1">Sign Up +</label>
                    <a href="#" class="drp">Sign Up</a>
                    <input type="checkbox" id="drop-1"/>
                    <ul>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>

                    </ul>

                </li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-2" class="toggle-2">Sign In +</label>
                    <a href="#">Sign In</a>
                    <input type="checkbox" id="drop-2"/>
                    <ul>
                        <li><a href="" target="_blank">Admin</a></li>
                        <li><a href="" target="_blank">Trainer</a></li>
                        <li><a href="" target="_blank">Student</a></li>
                    </ul>

                </li>
            </ul>
        </nav>
    </div>
    <div class="content">
        <div class="top-side">
            <div class="logo">
                <img src="resource/image/logo/logoo.png">
            </div>
            <div class="category">
                <nav class="nav">
                    <ul class="nav__menu">
                        <li class="nav__menu-item"><a>Course Category</a>
                            <ul class="nav__submenu">
                                <li class="nav__submenu-item"> <a href="">Programming Language C</a></li>
                                <li class="nav__submenu-item"> <a href="">Web Design Html & Css</a></li>
                                <li class="nav__submenu-item"> <a href="">Web Development PHP</a></li>
                                <li class="nav__submenu-item"> <a href="">JavaScript</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="search">
                <input type="search" placeholder="Enter Your Keyword" name="search-here" class="search-box">
                <button type="submit" name="submit" class="submit">Search</button>
            </div>
        </div>
        <div class="add">
            <script type="text/javascript" src="resource/js/jssor.slider.min.js"></script>
            <!-- use jssor.slider.debug.js instead for debug -->
            <script>
                jssor_1_slider_init = function() {

                    var jssor_1_SlideshowTransitions = [
                        {$Duration:1000,$Delay:80,$Cols:8,$Rows:4,$Clip:15,$SlideOut:true,$Easing:$Jease$.$OutQuad},
                        {$Duration:1200,y:0.3,$Cols:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                        {$Duration:1000,x:-1,y:2,$Rows:2,$Zoom:11,$Rotate:1,$SlideOut:true,$Assembly:2049,$ChessMode:{$Row:15},$Easing:{$Left:$Jease$.$InExpo,$Top:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.85}},
                        {$Duration:1200,x:4,$Cols:2,$Zoom:11,$SlideOut:true,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear},$Opacity:2},
                        {$Duration:1000,x:4,y:-4,$Zoom:11,$Rotate:1,$SlideOut:true,$Easing:{$Left:$Jease$.$InExpo,$Top:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.8}},
                        {$Duration:1500,x:0.3,y:-0.3,$Delay:80,$Cols:8,$Rows:4,$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$Easing:{$Left:$Jease$.$InJump,$Top:$Jease$.$InJump,$Clip:$Jease$.$OutQuad},$Round:{$Left:0.8,$Top:2.5}},
                        {$Duration:1000,x:-3,y:1,$Rows:2,$Zoom:11,$Rotate:1,$SlideOut:true,$Assembly:2049,$ChessMode:{$Row:28},$Easing:{$Left:$Jease$.$InExpo,$Top:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.7}},
                        {$Duration:1200,y:-1,$Cols:8,$Rows:4,$Clip:15,$During:{$Top:[0.5,0.5],$Clip:[0,0.5]},$Formation:$JssorSlideshowFormations$.$FormationStraight,$ChessMode:{$Column:12},$ScaleClip:0.5},
                        {$Duration:1000,x:0.5,y:0.5,$Zoom:1,$Rotate:1,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InCubic},$Opacity:2,$Round:{$Rotate:0.5}},
                        {$Duration:1200,x:-0.6,y:-0.6,$Zoom:1,$Rotate:1,$During:{$Left:[0.2,0.8],$Top:[0.2,0.8],$Zoom:[0.2,0.8],$Rotate:[0.2,0.8]},$Easing:{$Zoom:$Jease$.$Swing,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$Swing},$Opacity:2,$Round:{$Rotate:0.5}},
                        {$Duration:1500,y:-0.5,$Delay:60,$Cols:15,$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationCircle,$Easing:$Jease$.$InWave,$Round:{$Top:1.5}},
                        {$Duration:1000,$Delay:30,$Cols:8,$Rows:4,$Clip:15,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:2050,$Easing:$Jease$.$InQuad},
                        {$Duration:1200,$Delay:20,$Clip:3,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
                    ];

                    var jssor_1_SlideoTransitions = [
                        [{b:-1,d:1,rY:90},{b:8500,d:1000,o:-1,rY:-90}],
                        [{b:-1,d:1,o:-1},{b:0,d:480,x:-300,o:1},{b:1000,d:500,x:-370}],
                        [{b:480,d:520,x:100,y:-320}],
                        [{b:1500,d:500,y:250},{b:8500,d:1000,x:600}],
                        [{b:-1,d:1,o:-1,sX:-0.6,sY:-0.6},{b:2000,d:500,o:1,r:360,sX:0.6,sY:0.6},{b:8500,d:1000,y:-150}],
                        [{b:-1,d:1,o:-1,tZ:-200},{b:2500,d:1000,o:1,tZ:200},{b:3500,d:1000,o:-1,tZ:200}],
                        [{b:-1,d:1,o:-1,tZ:-200},{b:3500,d:1000,o:1,tZ:200},{b:4500,d:1000,o:-1,tZ:200}],
                        [{b:-1,d:1,o:-1,tZ:-200},{b:4500,d:1000,o:1,tZ:200},{b:5500,d:1000,o:-1,tZ:200}],
                        [{b:-1,d:1,o:-1,tZ:-200},{b:5500,d:1000,o:1,tZ:200},{b:6500,d:1000,o:-1,tZ:200}],
                        [{b:-1,d:1,o:-1,tZ:-200},{b:6500,d:1000,o:1,tZ:200},{b:7500,d:1000,o:-1,tZ:200}],
                        [{b:-1,d:1,o:-1,tZ:-200},{b:7500,d:1000,o:1,tZ:200},{b:8500,d:1000,o:-1,tZ:200}],
                        [{b:-1,d:1,c:{x:250.0,t:-250.0}},{b:0,d:1000,c:{x:-250.0,t:250.0}},{b:5000,d:1000,y:100}],
                        [{b:-1,d:1,o:-1},{b:1000,d:1000,o:1},{b:5000,d:1000,y:280}],
                        [{b:2000,d:1000,y:190,e:{y:28}},{b:5000,d:1000,x:280}],
                        [{b:3000,d:520,y:50},{b:5000,d:1000,y:-50}],
                        [{b:3520,d:480,x:-400},{b:5000,d:800,x:400,e:{x:7}}],
                        [{b:4000,d:500,x:-400},{b:5000,d:800,x:400,e:{x:7}}],
                        [{b:4500,d:500,x:-400},{b:5000,d:800,x:400,e:{x:7}}],
                        [{b:-1,d:1,c:{x:250.0,t:-250.0}},{b:0,d:1000,c:{x:-250.0,t:250.0}},{b:10000,d:500,y:90}],
                        [{b:-1,d:1,c:{x:150.0,t:-150.0}},{b:1000,d:1000,c:{x:-150.0,t:150.0}},{b:10000,d:500,c:{y:150.0,m:-150.0}}],
                        [{b:2000,d:500,x:220}],
                        [{b:3500,d:500,rX:-20},{b:4000,d:1000,rX:40},{b:5000,d:500,rX:-10},{b:9500,d:500,o:-1}],
                        [{b:3500,d:2000,r:360}],
                        [{b:-1,d:1,o:-1},{b:2500,d:500,x:76,y:-25,o:1}],
                        [{b:-1,d:1,o:-1},{b:2500,d:500,x:47,y:65,o:1}],
                        [{b:-1,d:1,o:-1},{b:2500,d:500,x:-47,y:65,o:1}],
                        [{b:-1,d:1,o:-1},{b:2500,d:500,x:-76,y:-25,o:1}],
                        [{b:-1,d:1,o:-1},{b:2500,d:500,y:-80,o:1}],
                        [{b:-1,d:1,c:{x:120.0,t:-120.0}},{b:6100,d:400,c:{x:-120.0,t:120.0}},{b:10000,d:500,y:-120}],
                        [{b:6500,d:500,x:220}],
                        [{b:-1,d:1,o:-1,r:180,sX:-0.5,sY:-0.5},{b:7000,d:500,o:1,r:180,sX:0.5,sY:0.5},{b:8000,d:500,o:-1,r:180,sX:9,sY:9}],
                        [{b:-1,d:1,o:-1,r:180,sX:-0.5,sY:-0.5},{b:8000,d:500,o:1,r:180,sX:0.5,sY:0.5},{b:9000,d:500,o:-1,r:180,sX:9,sY:9}],
                        [{b:-1,d:1,o:-1,r:180,sX:-0.5,sY:-0.5},{b:9000,d:500,o:1,r:180,sX:0.5,sY:0.5},{b:9500,d:500,o:-1,r:180,sX:9,sY:9}]
                    ];

                    var jssor_1_options = {
                        $AutoPlay: true,
                        $SlideDuration: 800,
                        $SlideEasing: $Jease$.$OutQuint,
                        $SlideshowOptions: {
                            $Class: $JssorSlideshowRunner$,
                            $Transitions: jssor_1_SlideshowTransitions,
                            $TransitionsOrder: 1
                        },
                        $CaptionSliderOptions: {
                            $Class: $JssorCaptionSlideo$,
                            $Transitions: jssor_1_SlideoTransitions,
                            $Breaks: [
                                [{d:3000,b:8500,t:2}],
                                [{d:2000,b:5000}],
                                [{d:3000,b:9500,t:2}]
                            ]
                        },
                        $ArrowNavigatorOptions: {
                            $Class: $JssorArrowNavigator$
                        },
                        $BulletNavigatorOptions: {
                            $Class: $JssorBulletNavigator$
                        }
                    };

                    var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

                    //responsive code begin
                    //you can remove responsive code if you don't want the slider scales while window resizing
                    function ScaleSlider() {
                        var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                        if (refSize) {
                            refSize = Math.min(refSize, 1350);
                            jssor_1_slider.$ScaleWidth(refSize);
                        }
                        else {
                            window.setTimeout(ScaleSlider, 30);
                        }
                    }
                    ScaleSlider();
                    $Jssor$.$AddEvent(window, "load", ScaleSlider);
                    $Jssor$.$AddEvent(window, "resize", ScaleSlider);
                    $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
                    //responsive code end
                };
            </script>
            <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1350px; height: 380px; overflow: hidden; visibility: hidden;">
                <!-- Loading Screen -->
                <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                    <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                    <div style="position:absolute;display:block;background:url('resource/image/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                </div>
                <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1350px; height: 380px; overflow: hidden;">
                    <div data-b="0" data-p="170.00" data-po="80% 55%" style="display: none;">
                        <img data-u="image" src="resource/image/img/virtualclass1.png" />
                        <div data-u="caption" data-t="3" style="position: absolute; top: -50px; left: 350px; width: 150px; height: 40px; background-color: rgba(0,0,0,0.7); font-size: 16px; color: #ffffff; line-height: 40px; text-align: center;">Virtual Room</div>
                        <div data-u="caption" data-t="3" style="position: absolute; top: -100px; left: 350px; width: 80px; height: 40px; background-color: rgba(0,0,0,0.7); font-size: 16px; color: #ffffff; line-height: 40px; text-align: center;">To</div>
                        <div data-u="caption" data-t="4" style="position: absolute; top: 100px; left: 350px; width: 150px; height: 40px; background-color: rgba(0,0,0,0.7); font-size: 16px; color: #ffffff; line-height: 40px; text-align: center;">Welcome</div>
                        <div data-u="caption" data-t="5" data-3d="1" style="position: absolute; top: 200px; left: 700px; width: 200px; height: 80px; background-color: rgba(0,0,0,0.7); font-size: 18px; color: #ffffff; line-height: 100px; text-align: center;">Programming Language</div>
                        <div data-u="caption" data-t="6" data-3d="1" style="position: absolute; top: 200px; left: 700px; width: 200px; height: 80px; background-color: rgba(0,0,0,0.7); font-size: 18px; color: #ffffff; line-height: 100px; text-align: center;">Web Designing</div>
                        <div data-u="caption" data-t="7" data-3d="1" style="position: absolute; top: 200px; left: 700px; width: 200px; height: 80px; background-color: rgba(0,0,0,0.7); font-size: 18px; color: #ffffff; line-height: 100px; text-align: center;">Web Development</div>
                        <div data-u="caption" data-t="8" data-3d="1" style="position: absolute; top: 200px; left: 700px; width: 200px; height: 80px; background-color: rgba(0,0,0,0.7); font-size: 18px; color: #ffffff; line-height: 100px; text-align: center;">Graphics Design</div>
                        <div data-u="caption" data-t="9" data-3d="1" style="position: absolute; top: 200px; left: 700px; width: 200px; height: 80px; background-color: rgba(0,0,0,0.7); font-size: 18px; color: #ffffff; line-height: 100px; text-align: center;">Networking</div>
                        <div data-u="caption" data-t="10" data-3d="1" style="position: absolute; top: 200px; left: 700px; width: 200px; height: 80px; background-color: rgba(0,0,0,0.7); font-size: 18px; color: #ffffff; line-height: 100px; text-align: center;">What Do You Want??</div>
                    </div>
                    <div data-b="1" data-p="170.00" style="display: none;">
                        <img data-u="image" src="resource/image/img/class1.png" />
                        <div data-u="caption" data-t="11" style="position: absolute; top: 300px; left: 20px; width: 500px; height: 30px; background-color: rgba(0,0,0,.7); font-size: 20px; color: #ffffff; line-height: 30px; text-align: center;">Most Popular Language Of the World</div>
                        <div data-u="caption" data-t="12" style="position: absolute; top: 120px; left: 930px; width: 200px; height: 90px; background-color: rgba(0,0,0,0.5); font-size: 16px; color: #ffffff; line-height: 28px; text-align: center; padding: 2px; box-sizing: border-box;">Make<br />
                            All<br />
                            Easy For You
                        </div>
                        <div style="position: absolute; top: 40px; left: 300px; width: 400px; height: 200px; overflow: hidden;">
                            <div data-u="caption" data-t="14" style="position: absolute; top: -50px; left: 35px; width: 210px; height: 30px; font-size: 18px; color: #000000; line-height: 30px;">Language&nbsp; &nbsp; &nbsp;  Popularity</div>
                            <div data-u="caption" data-t="15" style="position: absolute; top: 50px; left: 400px; width: 210px; height: 30px; font-size: 18px; color: #000; line-height: 30px;">JavaScript &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; ~97%</div>
                            <div data-u="caption" data-t="16" style="position: absolute; top: 100px; left: 400px; width: 210px; height: 30px; font-size: 18px; color: #000; line-height: 30px;">Java&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ~63%</div>
                            <div data-u="caption" data-t="17" style="position: absolute; top: 150px; left: 400px; width: 210px; height: 30px; font-size: 18px; color: #000; line-height: 30px;">Python&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ~54%</div>
                        </div>
                    </div>


                </div>
                <!-- Bullet Navigator -->
                <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
                    <!-- bullet navigator item prototype -->
                    <div data-u="prototype" style="width:16px;height:16px;"></div>
                </div>
                <!-- Arrow Navigator -->
                <span data-u="arrowleft" class="jssora22l" style="top:0px;left:10px;width:40px;height:58px;" data-autocenter="2"></span>
                <span data-u="arrowright" class="jssora22r" style="top:0px;right:10px;width:40px;height:58px;" data-autocenter="2"></span>
            </div>
            <script>
                jssor_1_slider_init();
            </script>
        </div>
        <div class="mid-side">
            <div class="our-ministry">
                <h1>About Us</h1>
                <article>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                    Vestibulum congue placerat ligula, sed dictum lectus tempor nec.
                    Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                    Vestibulum at odio eget eros lacinia venenatis Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit.
                    Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                    Vestibulum congue placerat ligula, sed dictum lectus tempor nec.
                    Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                    Vestibulum at odio eget eros lacinia venenatis sed dictum lectus tempor nec.
                    Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                    Vestibulum at odio eget eros lacinia venenatis sed dictum lectus tempor nec.
                    Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                    Vestibulum at odio eget eros lacinia venenatis Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                    Vestibulum congue placerat ligula, sed dictum lectus tempor nec.
                    Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                    Vestibulum at odio eget eros lacinia venenatis Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit.
                    Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                    Vestibulum congue placerat ligula, sed dictum lectus tempor nec.
                    Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                </article>
                <div class="btn-op">
                    <input type="button" class="btn" name="btn" value="Learn More  >">
                </div>
            </div>
            <div class="features">
                <div class="figure">
                    <ul>
                        <li><a href=""><i class="icon icon-user"></i></a></li>
                    </ul>
                    <div class="fig-caption">
                        <h2>Admin</h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                            Vestibulum congue placerat ligula,
                        </p>
                        <div class="btn-lo">
                            <input type="button" class="btn-1" name="btn-1" value="Log In" onclick="window.location='views/Admin/Authentication/login.php'">
                        </div>
                    </div>
                </div>
                <div class="figure">
                    <ul>
                        <li><a href=""><i class="icon icon-user-plus"></i></a></li>
                    </ul>
                    <div class="fig-caption">
                        <h2>Register</h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                            Vestibulum congue placerat ligula,
                        </p>
                        <div class="btn-lo">
                            <input type="button" class="btn-1" name="btn-1" value="Join Us +">
                        </div>
                    </div>
                </div>
                <div class="figure">
                    <ul>
                        <li><a href=""><i class="icon icon-user-tie"></i></a></li>
                    </ul>
                    <div class="fig-caption">
                        <h2>Trainer Panel</h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                            Vestibulum congue placerat ligula,
                        </p>
                        <div class="btn-lo">
                            <input type="button" class="btn-1" name="btn-1" value="Join Us +">
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide">
                <div class="slidey">
                    <div class="slider">
                        <div class="slider-caption">
                            <article>
                            <h1>Lorem ipsum dolor sit amet</h1>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                                Vestibulum congue placerat ligula, sed dictum lectus tempor nec.
                                Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                                Vestibulum at odio eget eros lacinia venenatis Lorem ipsum dolor sit amet,
                                consectetur adipiscing elit.
                                Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                                Vestibulum congue placerat ligula, sed dictum lectus tempor nec.
                                Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                                Vestibulum at odio eget eros lacinia venenatis sed dictum lectus tempor nec.
                                Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                                Vestibulum at odio eget eros lacinia venenatis sed dictum lectus tempor nec.
                                Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                                Vestibulum at odio eget eros lacinia venenatis
                            </article>
                        </div>
                    </div>
                    <div class="slider">
                        <div class="slider-caption">
                            <article>
                                <h1>Lorem ipsum dolor sit amet</h1>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                                Vestibulum congue placerat ligula, sed dictum lectus tempor nec.
                                Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                                Vestibulum at odio eget eros lacinia venenatis Lorem ipsum dolor sit amet,
                                consectetur adipiscing elit.
                                Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                                Vestibulum congue placerat ligula, sed dictum lectus tempor nec.
                                Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                                Vestibulum at odio eget eros lacinia venenatis sed dictum lectus tempor nec.
                                Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                                Vestibulum at odio eget eros lacinia venenatis sed dictum lectus tempor nec.
                                Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                                Vestibulum at odio eget eros lacinia venenatis
                            </article>
                        </div>
                    </div>
                    <div class="slider">
                        <div class="slider-caption">
                            <article>
                                <h1>Lorem ipsum dolor sit amet</h1>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                                Vestibulum congue placerat ligula, sed dictum lectus tempor nec.
                                Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                                Vestibulum at odio eget eros lacinia venenatis Lorem ipsum dolor sit amet,
                                consectetur adipiscing elit.
                                Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                                Vestibulum congue placerat ligula, sed dictum lectus tempor nec.
                                Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                                Vestibulum at odio eget eros lacinia venenatis sed dictum lectus tempor nec.
                                Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                                Vestibulum at odio eget eros lacinia venenatis sed dictum lectus tempor nec.
                                Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                                Vestibulum at odio eget eros lacinia venenatis
                            </article>
                        </div>
                    </div>
                    <div class="slider">
                        <div class="slider-caption">
                            <article>
                                <h1>Lorem ipsum dolor sit amet</h1>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                                Vestibulum congue placerat ligula, sed dictum lectus tempor nec.
                                Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                                Vestibulum at odio eget eros lacinia venenatis Lorem ipsum dolor sit amet,
                                consectetur adipiscing elit.
                                Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                                Vestibulum congue placerat ligula, sed dictum lectus tempor nec.
                                Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                                Vestibulum at odio eget eros lacinia venenatis sed dictum lectus tempor nec.
                                Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                                Vestibulum at odio eget eros lacinia venenatis sed dictum lectus tempor nec.
                                Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                                Vestibulum at odio eget eros lacinia venenatis
                            </article>
                        </div>
                    </div>
                </div>
            </div>
            <div class="some-text">
                <div class="text-i">
                    <article>
                        <h1>Lorem ipsum dolor sit amet</h1>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                        Vestibulum congue placerat ligula, sed dictum lectus tempor nec.
                        Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                        Vestibulum at odio eget eros lacinia venenatis Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit.
                        Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                        Vestibulum congue placerat ligula, sed dictum lectus tempor nec.
                        Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                        Vestibulum at odio eget eros lacinia venenatis sed dictum lectus tempor nec.
                        Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                        Vestibulum at odio eget eros lacinia venenatis sed dictum lectus tempor nec.
                        Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                        Vestibulum at odio eget eros lacinia venenatis
                        Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                        Vestibulum congue placerat ligula, sed dictum lectus tempor nec.
                        Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                        Vestibulum at odio eget eros lacinia venenatis sed dictum lectus tempor nec.
                        Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                        Vestibulum at odio eget eros lacinia venenatis sed dictum lectus tempor nec.
                        Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                        Vestibulum at odio eget eros lacinia venenatis
                        Duis vel dolor vitae mauris aliquam feugiat a sed ipsum.
                        Vestibulum congue placerat ligula, sed dictum lectus tempor nec.
                        Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                        Vestibulum at odio eget eros lacinia venenatis sed dictum lectus tempor nec.
                        Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                        Vestibulum at odio eget eros lacinia venenatis sed dictum lectus tempor nec.
                        Aenean at tellus auctor, ultricies leo in, pellentesque enim.
                        Vestibulum at odio eget eros lacinia venenatis
                    </article>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="part">
            <div class="part-1">
                <img src="resource/image/logo/vc.png">
            </div>
            <div class="part-2">
                <div class="contact-us">
                    <h1>Contact Us</h1>
                    <div class="info">
                        <p>BSPI Computer Engineering Project.</p>
                        <p>7th Semister, 2nd Shift.</p>
                        <p>Computer Department,</p>
                        <p>Bangladesh Sweden Polytechnic Institute.</p>
                        <p>Tel : 01752-111111, 01631-222222</p>
                        <p><a href="">https://virtualclass-new.com</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="part-3">
            <div class="social">
                <ul>
                    <li><a href=""><i class="icon icon-facebook"></i></a></li>
                    <li><a href=""><i class="icon icon-twitter"></i></a></li>
                    <li><a href=""><i class="icon icon-instagram"></i></a></li>
                    <li><a href=""><i class="icon icon-youtube"></i></a></li>
                    <li><a href=""><i class="icon icon-google-plus3"></i></a></li>
                </ul>
            </div>
        </div>
    </div>


</div>
</body>
</html>



